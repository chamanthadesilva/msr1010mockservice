package sysco.cake.mock.api.service;

/**
 * Created by supun on 1/20/17.
 */
public final class XmlQuarterlyResponses {

    private XmlQuarterlyResponses(){}

    public static String TOTAL_DELIVERY = "<out>\n" +
            "    <rc>0</rc>\n" +
            "    <msg>querydata successful</msg>\n" +
            "    <table>\n" +
            "        <totals>1</totals>\n" +
            "        <cols>\n" +
            "            <th name=\"col_name\" type=\"a\" fixed=\"1\">col_name</th>\n" +
            "            <th name=\"value\" type=\"i\" format=\"type:num\" fixed=\"0\">Ucnt&#10;Route&#10;Number</th>\n" +
            "        </cols>\n" +
            "        <data>\n" +
            "            <tr>\n" +
            "                <td>Previos_range</td>\n" +
            "                <td>4355</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>Current_range</td>\n" +
            "                <td>4624</td>\n" +
            "            </tr>\n" +
            "        </data>\n" +
            "    </table>\n" +
            "    <nrows>2</nrows>\n" +
            "</out>";

    public static String TOTAL_INVOICE = "<out>\n" +
            "    <rc>0</rc>\n" +
            "    <msg>querydata successful</msg>\n" +
            "    <table>\n" +
            "        <totals>1</totals>\n" +
            "        <cols>\n" +
            "            <th name=\"col_name\" type=\"a\" fixed=\"1\">col_name</th>\n" +
            "            <th name=\"value\" type=\"i\" format=\"type:num\" fixed=\"0\">Ucnt&#10;Obligation&#10;Number</th>\n" +
            "        </cols>\n" +
            "        <data>\n" +
            "            <tr>\n" +
            "                <td>Previos_range</td>\n" +
            "                <td>105924</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>Current_range</td>\n" +
            "                <td>140517</td>\n" +
            "            </tr>\n" +
            "        </data>\n" +
            "    </table>\n" +
            "    <nrows>2</nrows>\n" +
            "</out>";

    public static String TOTAL_PURCHASE = "<out>\n" +
            "    <rc>0</rc>\n" +
            "    <msg>querydata successful</msg>\n" +
            "    <table>\n" +
            "        <totals>1</totals>\n" +
            "        <cols>\n" +
            "            <th name=\"col_name\" type=\"a\" fixed=\"1\">col_name</th>\n" +
            "            <th name=\"value\" type=\"f\" format=\"type:currency\" fixed=\"0\">Sum&#10;Net&#10;Sales $</th>\n" +
            "        </cols>\n" +
            "        <data>\n" +
            "            <tr>\n" +
            "                <td>Previos_range</td>\n" +
            "                <td>89149836.4099964</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>Current_range</td>\n" +
            "                <td>118915258.569998</td>\n" +
            "            </tr>\n" +
            "        </data>\n" +
            "    </table>\n" +
            "    <nrows>2</nrows>\n" +
            "</out>";
    public static String TOP_CATEGORIES = "<out>\n" +
            "    <rc>0</rc>\n" +
            "    <msg>querydata successful</msg>\n" +
            "    <table>\n" +
            "        <totals>1</totals>\n" +
            "        <cols>\n" +
            "            <th name=\"netSalesPercent\" type=\"f\" format=\"type:num;width:5;dec:0\" fixed=\"0\">PGsum&#10;Net&#10;Sales $</th>\n" +
            "            <th name=\"netSales\" type=\"f\" format=\"type:num;width:15;dec:2\" fixed=\"0\">Sum&#10;Net&#10;Sales $</th>\n" +
            "            <th name=\"catName\" type=\"a\" format=\"type:char;width:50\" fixed=\"0\">catName</th>\n" +
            "            <th name=\"catId\" type=\"i\" format=\"type:nocommas;width:10\" fixed=\"0\">catId</th>\n" +
            "        </cols>\n" +
            "        <data>\n" +
            "            <tr>\n" +
            "                <td>31.5800041600395</td>\n" +
            "                <td>33244181.9900002</td>\n" +
            "                <td>FROZEN              </td>\n" +
            "                <td>6</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>24.2666480193044</td>\n" +
            "                <td>25545432.4500008</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>7</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>12.7970181605148</td>\n" +
            "                <td>13471385.1999999</td>\n" +
            "                <td>POULTRY             </td>\n" +
            "                <td>5</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>11.3124066232496</td>\n" +
            "                <td>11908538.7900001</td>\n" +
            "                <td>PAPER &amp; DISP        </td>\n" +
            "                <td>8</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>6.44504847444296</td>\n" +
            "                <td>6784684.49</td>\n" +
            "                <td>PRODUCE             </td>\n" +
            "                <td>11</td>\n" +
            "            </tr>\n" +
            "        </data>\n" +
            "    </table>\n" +
            "    <nrows>5</nrows>\n" +
            "</out>";

    public static String PURCHASE_DATA = "<out>\n" +
            "    <rc>0</rc>\n" +
            "    <msg>querydata successful</msg>\n" +
            "    <table>\n" +
            "        <totals>1</totals>\n" +
            "        <cols>\n" +
            "            <th name=\"oblig_dt\" type=\"i\" format=\"type:ansidate\" fixed=\"1\">Obligation&#10;Invoice&#10;Date</th>\n" +
            "            <th name=\"catgy_id\" type=\"i\" format=\"type:nocommas;width:10\" fixed=\"1\">Category ID</th>\n" +
            "            <th name=\"ec_category_description\" type=\"a\" format=\"type:char;width:50\" fixed=\"1\">Category&#10;Description</th>\n" +
            "            <th name=\"itm_nbr\" type=\"a\" format=\"type:char;width:20\" fixed=\"1\">Item&#10;Number</th>\n" +
            "            <th name=\"itm_desc_sus_itm\" type=\"a\" format=\"type:char;width:20\" fixed=\"1\">Item&#10;Description</th>\n" +
            "            <th name=\"brnd_cd_sus_itm\" type=\"a\" format=\"type:char;width:20\" fixed=\"1\">Brand&#10;Code</th>\n" +
            "            <th name=\"bic_zpaksize_sap_s_0material_attr\" type=\"a\" format=\"type:char;width:20\" fixed=\"1\">Pack/Size</th>\n" +
            "            <th name=\"itm_catch_wgt_ind\" type=\"a\" format=\"type:char;width:10\" fixed=\"1\">Item Catch&#10;Weight&#10;Indicator</th>\n" +
            "            <th name=\"case_sold_qty\" type=\"i\" format=\"type:nocommas;width:10\" fixed=\"1\">Full &#10;Cases&#10;Sold</th>\n" +
            "            <th name=\"caseequiv\" type=\"f\" format=\"type:num;width:13;dec:2\" fixed=\"0\">Sum&#10;Case&#10;Equivalents&#10;Sold</th>\n" +
            "            <th name=\"purchasedol\" type=\"f\" format=\"type:num;width:15;dec:2\" fixed=\"0\">Sum&#10;Net&#10;Sales $</th>\n" +
            "        </cols>\n" +
            "        <data>\n" +
            "            <tr>\n" +
            "                <td>20150801</td>\n" +
            "                <td>11</td>\n" +
            "                <td>PRODUCE             </td>\n" +
            "                <td>1167709</td>\n" +
            "                <td>TOMATO 2 LAYER FRESH 6X6</td>\n" +
            "                <td>SYS IMP</td>\n" +
            "                <td>1/72CT</td>\n" +
            "                <td>N</td>\n" +
            "                <td>-1</td>\n" +
            "                <td>-1</td>\n" +
            "                <td>-26.85</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20150801</td>\n" +
            "                <td>8</td>\n" +
            "                <td>PAPER &amp; DISP        </td>\n" +
            "                <td>5989878</td>\n" +
            "                <td>GLOVE VINYL FDSV PF MED</td>\n" +
            "                <td>SYS CLS</td>\n" +
            "                <td>4/100CT</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>1</td>\n" +
            "                <td>26.78</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20150801</td>\n" +
            "                <td>6</td>\n" +
            "                <td>FROZEN              </td>\n" +
            "                <td>9669136</td>\n" +
            "                <td>SAUCE TZATZIKI SOUR CREAM</td>\n" +
            "                <td>MKZ CLS</td>\n" +
            "                <td>4/.5GAL</td>\n" +
            "                <td>N</td>\n" +
            "                <td>0</td>\n" +
            "                <td>0.75</td>\n" +
            "                <td>23.17</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20150801</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>5358181</td>\n" +
            "                <td>SUGAR GRANULATED GDN RAW CANE</td>\n" +
            "                <td>FLORCRY</td>\n" +
            "                <td>1/50LB</td>\n" +
            "                <td>N</td>\n" +
            "                <td>2</td>\n" +
            "                <td>18</td>\n" +
            "                <td>647.2</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20150801</td>\n" +
            "                <td>6</td>\n" +
            "                <td>FROZEN              </td>\n" +
            "                <td>7222843</td>\n" +
            "                <td>BREAD PANINI PREGRILLED 7&quot;</td>\n" +
            "                <td>ALLADN</td>\n" +
            "                <td>12/10EA</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>13</td>\n" +
            "                <td>469.43</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20150801</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>3587774</td>\n" +
            "                <td>SAUCE PINEAPPLE SRIRACHA RSTD</td>\n" +
            "                <td>GIRARD</td>\n" +
            "                <td>6/16OZ</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>4</td>\n" +
            "                <td>101.12</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20150801</td>\n" +
            "                <td>11</td>\n" +
            "                <td>PRODUCE             </td>\n" +
            "                <td>1679919</td>\n" +
            "                <td>BASIL FRESH</td>\n" +
            "                <td>PACKER</td>\n" +
            "                <td>1/4OZ</td>\n" +
            "                <td>N</td>\n" +
            "                <td>-1</td>\n" +
            "                <td>-1</td>\n" +
            "                <td>-3.68</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20150801</td>\n" +
            "                <td>11</td>\n" +
            "                <td>PRODUCE             </td>\n" +
            "                <td>7350788</td>\n" +
            "                <td>ONION GREEN ICELS</td>\n" +
            "                <td>SYS NAT</td>\n" +
            "                <td>4/2LB</td>\n" +
            "                <td>N</td>\n" +
            "                <td>0</td>\n" +
            "                <td>1.75</td>\n" +
            "                <td>38.75</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20150801</td>\n" +
            "                <td>6</td>\n" +
            "                <td>FROZEN              </td>\n" +
            "                <td>1149602</td>\n" +
            "                <td>BREAD CIABATTA SOFT 4X4 SLI</td>\n" +
            "                <td>BURRY</td>\n" +
            "                <td>64/3.5OZ</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>16</td>\n" +
            "                <td>339.2</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20150801</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>6744102</td>\n" +
            "                <td>CHIP MULTIGRAIN ORIG BIG GRAB</td>\n" +
            "                <td>SUNCHIP</td>\n" +
            "                <td>64/1.5OZ</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>4</td>\n" +
            "                <td>113.72</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20150801</td>\n" +
            "                <td>6</td>\n" +
            "                <td>FROZEN              </td>\n" +
            "                <td>2194009</td>\n" +
            "                <td>BLUEBERRY IQF</td>\n" +
            "                <td>PACKER</td>\n" +
            "                <td>1/30LB</td>\n" +
            "                <td>N</td>\n" +
            "                <td>2</td>\n" +
            "                <td>6</td>\n" +
            "                <td>279.28</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20150801</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>6725737</td>\n" +
            "                <td>CHIP POTATO JALAPENO KETTLE</td>\n" +
            "                <td>MSVICKI</td>\n" +
            "                <td>64/1.375Z</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>6</td>\n" +
            "                <td>180.54</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20150801</td>\n" +
            "                <td>3</td>\n" +
            "                <td>MEATS               </td>\n" +
            "                <td>1916996</td>\n" +
            "                <td>BEEF STEAK STRIP SPCY FC</td>\n" +
            "                <td>NSP</td>\n" +
            "                <td>4/5LB</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>4</td>\n" +
            "                <td>548.59</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20150801</td>\n" +
            "                <td>11</td>\n" +
            "                <td>PRODUCE             </td>\n" +
            "                <td>2112035</td>\n" +
            "                <td>KALE FRESH ICELS</td>\n" +
            "                <td>PACKER</td>\n" +
            "                <td>1/24CT</td>\n" +
            "                <td>N</td>\n" +
            "                <td>-1</td>\n" +
            "                <td>-1</td>\n" +
            "                <td>-14.83</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20150801</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>4106498</td>\n" +
            "                <td>PINEAPPLE TIDBIT JCE FCY</td>\n" +
            "                <td>DOLE</td>\n" +
            "                <td>6/#10</td>\n" +
            "                <td>N</td>\n" +
            "                <td>4</td>\n" +
            "                <td>24</td>\n" +
            "                <td>755.04</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20150801</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>2489710</td>\n" +
            "                <td>DRESSING RANCH LITE</td>\n" +
            "                <td>TRPSMTH</td>\n" +
            "                <td>2/1GAL</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>10</td>\n" +
            "                <td>184.5</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20150801</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>6743530</td>\n" +
            "                <td>CHIP POTATO SALT &amp; VNGR KETTLE</td>\n" +
            "                <td>MSVICKI</td>\n" +
            "                <td>64/1.375Z</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>7</td>\n" +
            "                <td>210.63</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20150801</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>9477498</td>\n" +
            "                <td>ALLOWANCE FOR DROP SIZE</td>\n" +
            "                <td>NONPROD</td>\n" +
            "                <td></td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>4</td>\n" +
            "                <td>-29.96</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20150801</td>\n" +
            "                <td>8</td>\n" +
            "                <td>PAPER &amp; DISP        </td>\n" +
            "                <td>5889407</td>\n" +
            "                <td>NAPKIN DISP XPRESSNP NAT</td>\n" +
            "                <td>TORKUNV</td>\n" +
            "                <td>12/500CT</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>1</td>\n" +
            "                <td>36.74</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20150801</td>\n" +
            "                <td>11</td>\n" +
            "                <td>PRODUCE             </td>\n" +
            "                <td>1158542</td>\n" +
            "                <td>BANANA FRESH TIP GREEN</td>\n" +
            "                <td>PACKER</td>\n" +
            "                <td>1/40LB</td>\n" +
            "                <td>N</td>\n" +
            "                <td>-2</td>\n" +
            "                <td>-2</td>\n" +
            "                <td>-40.76</td>\n" +
            "            </tr>\n" +
            "        </data>\n" +
            "    </table>\n" +
            "    <nrows>576810</nrows>\n" +
            "</out>";

    public static String TOP5_CATEGORY = "<out>\n" +
            "    <rc>0</rc>\n" +
            "    <msg>querydata successful</msg>\n" +
            "    <table>\n" +
            "        <totals>1</totals>\n" +
            "        <cols>\n" +
            "            <th name=\"date_name\" type=\"a\" fixed=\"1\">First&#10;date_name_temp</th>\n" +
            "            <th name=\"m0\" type=\"f\" format=\"type:currency\" fixed=\"0\">Category&#10;Description=PRODUCE                                           &#10;col_sort_order=1</th>\n" +
            "            <th name=\"m1\" type=\"f\" format=\"type:currency\" fixed=\"0\">Category&#10;Description=PAPER &amp; DISP                                      &#10;col_sort_order=2</th>\n" +
            "            <th name=\"m2\" type=\"f\" format=\"type:currency\" fixed=\"0\">Category&#10;Description=POULTRY                                           &#10;col_sort_order=3</th>\n" +
            "            <th name=\"m3\" type=\"f\" format=\"type:currency\" fixed=\"0\">Category&#10;Description=CANNED AND DRY                                    &#10;col_sort_order=4</th>\n" +
            "            <th name=\"m4\" type=\"f\" format=\"type:currency\" fixed=\"0\">Category&#10;Description=FROZEN                                            &#10;col_sort_order=5</th>\n" +
            "        </cols>\n" +
            "        <data>\n" +
            "            <tr>\n" +
            "                <td>2015 August-Aug  &apos;15</td>\n" +
            "                <td>752233.819999997</td>\n" +
            "                <td>1284717.38</td>\n" +
            "                <td>1468718.8</td>\n" +
            "                <td>2495949.13999996</td>\n" +
            "                <td>3713051.23999998</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>2015 October-Oct  &apos;15</td>\n" +
            "                <td>884176.850000008</td>\n" +
            "                <td>1562476.2</td>\n" +
            "                <td>1938811.54999999</td>\n" +
            "                <td>3379022.00999995</td>\n" +
            "                <td>4688628.03999996</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>2016 January-Jan  &apos;16</td>\n" +
            "                <td>1069391.05</td>\n" +
            "                <td>1866507.95</td>\n" +
            "                <td>2071259.93</td>\n" +
            "                <td>4001821.21999998</td>\n" +
            "                <td>5264559.13000003</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>2016 April-Apr  &apos;16</td>\n" +
            "                <td>1366023.98000002</td>\n" +
            "                <td>2401550.74</td>\n" +
            "                <td>2545400.1</td>\n" +
            "                <td>5232317.40000007</td>\n" +
            "                <td>7004579.30000005</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>2016 July-Jul  &apos;16</td>\n" +
            "                <td>1352935.66</td>\n" +
            "                <td>2374094.12999999</td>\n" +
            "                <td>2646051.52</td>\n" +
            "                <td>5135045.29000008</td>\n" +
            "                <td>6567757.49000001</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>2016 October-Oct  &apos;16</td>\n" +
            "                <td>1087201.98</td>\n" +
            "                <td>1929884.37999999</td>\n" +
            "                <td>2230725.23</td>\n" +
            "                <td>4243562.54000002</td>\n" +
            "                <td>4796246.22999998</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>2017 January-Jan  &apos;17</td>\n" +
            "                <td>272721.15</td>\n" +
            "                <td>489308.01</td>\n" +
            "                <td>570418.07</td>\n" +
            "                <td>1057714.85</td>\n" +
            "                <td>1209360.56</td>\n" +
            "            </tr>\n" +
            "        </data>\n" +
            "    </table>\n" +
            "    <nrows>7</nrows>\n" +
            "</out>";
}
