package sysco.cake.mock.api.service;

/**
 * Created by supun on 1/20/17.
 */
public final class XmlYearlyResponses {

    private XmlYearlyResponses(){}

    public static String TOTAL_DELIVERY = "<out>\n" +
            "    <rc>0</rc>\n" +
            "    <msg>querydata successful</msg>\n" +
            "    <table>\n" +
            "        <totals>1</totals>\n" +
            "        <cols>\n" +
            "            <th name=\"col_name\" type=\"a\" fixed=\"1\">col_name</th>\n" +
            "            <th name=\"value\" type=\"i\" format=\"type:num\" fixed=\"0\">Ucnt&#10;Route&#10;Number</th>\n" +
            "        </cols>\n" +
            "        <data>\n" +
            "            <tr>\n" +
            "                <td>Previos_range</td>\n" +
            "                <td>5018</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>Current_range</td>\n" +
            "                <td>6840</td>\n" +
            "            </tr>\n" +
            "        </data>\n" +
            "    </table>\n" +
            "    <nrows>2</nrows>\n" +
            "</out>";

    public static String TOTAL_INVOICE = "<out>\n" +
            "    <rc>0</rc>\n" +
            "    <msg>querydata successful</msg>\n" +
            "    <table>\n" +
            "        <totals>1</totals>\n" +
            "        <cols>\n" +
            "            <th name=\"col_name\" type=\"a\" fixed=\"1\">col_name</th>\n" +
            "            <th name=\"value\" type=\"i\" format=\"type:num\" fixed=\"0\">Ucnt&#10;Obligation&#10;Number</th>\n" +
            "        </cols>\n" +
            "        <data>\n" +
            "            <tr>\n" +
            "                <td>Previos_range</td>\n" +
            "                <td>92890</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>Current_range</td>\n" +
            "                <td>270651</td>\n" +
            "            </tr>\n" +
            "        </data>\n" +
            "    </table>\n" +
            "    <nrows>2</nrows>\n" +
            "</out>";

    public static String TOTAL_PURCHASE = "<out>\n" +
            "    <rc>0</rc>\n" +
            "    <msg>querydata successful</msg>\n" +
            "    <table>\n" +
            "        <totals>1</totals>\n" +
            "        <cols>\n" +
            "            <th name=\"col_name\" type=\"a\" fixed=\"1\">col_name</th>\n" +
            "            <th name=\"value\" type=\"f\" format=\"type:currency\" fixed=\"0\">Sum&#10;Net&#10;Sales $</th>\n" +
            "        </cols>\n" +
            "        <data>\n" +
            "            <tr>\n" +
            "                <td>Previos_range</td>\n" +
            "                <td>72217752.9999975</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>Current_range</td>\n" +
            "                <td>228333434.179995</td>\n" +
            "            </tr>\n" +
            "        </data>\n" +
            "    </table>\n" +
            "    <nrows>2</nrows>\n" +
            "</out>";

    public static String TOP_CATEGORIES = "<out>\n" +
            "    <rc>0</rc>\n" +
            "    <msg>querydata successful</msg>\n" +
            "    <table>\n" +
            "        <totals>1</totals>\n" +
            "        <cols>\n" +
            "            <th name=\"netSalesPercent\" type=\"f\" format=\"type:num;width:5;dec:0\" fixed=\"0\">PGsum&#10;Net&#10;Sales $</th>\n" +
            "            <th name=\"netSales\" type=\"f\" format=\"type:num;width:15;dec:2\" fixed=\"0\">Sum&#10;Net&#10;Sales $</th>\n" +
            "            <th name=\"catName\" type=\"a\" format=\"type:char;width:50\" fixed=\"0\">catName</th>\n" +
            "            <th name=\"catId\" type=\"i\" format=\"type:nocommas;width:10\" fixed=\"0\">catId</th>\n" +
            "        </cols>\n" +
            "        <data>\n" +
            "            <tr>\n" +
            "                <td>31.1460770465186</td>\n" +
            "                <td>62871081.2800005</td>\n" +
            "                <td>FROZEN              </td>\n" +
            "                <td>6</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>24.2763037546875</td>\n" +
            "                <td>49003842.9000015</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>7</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>12.3704151029299</td>\n" +
            "                <td>24970765.0899998</td>\n" +
            "                <td>POULTRY             </td>\n" +
            "                <td>5</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>11.5338136705071</td>\n" +
            "                <td>23282011.9100002</td>\n" +
            "                <td>PAPER &amp; DISP        </td>\n" +
            "                <td>8</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>6.43638227101243</td>\n" +
            "                <td>12992400.6899998</td>\n" +
            "                <td>PRODUCE             </td>\n" +
            "                <td>11</td>\n" +
            "            </tr>\n" +
            "        </data>\n" +
            "    </table>\n" +
            "    <nrows>5</nrows>\n" +
            "</out>";

    public static String PURCHASE_DATA = "<out>\n" +
            "    <rc>0</rc>\n" +
            "    <msg>querydata successful</msg>\n" +
            "    <table>\n" +
            "        <totals>1</totals>\n" +
            "        <cols>\n" +
            "            <th name=\"oblig_dt\" type=\"i\" format=\"type:ansidate\" fixed=\"1\">Obligation&#10;Invoice&#10;Date</th>\n" +
            "            <th name=\"catgy_id\" type=\"i\" format=\"type:nocommas;width:10\" fixed=\"1\">Category ID</th>\n" +
            "            <th name=\"ec_category_description\" type=\"a\" format=\"type:char;width:50\" fixed=\"1\">Category&#10;Description</th>\n" +
            "            <th name=\"itm_nbr\" type=\"a\" format=\"type:char;width:20\" fixed=\"1\">Item&#10;Number</th>\n" +
            "            <th name=\"itm_desc_sus_itm\" type=\"a\" format=\"type:char;width:20\" fixed=\"1\">Item&#10;Description</th>\n" +
            "            <th name=\"brnd_cd_sus_itm\" type=\"a\" format=\"type:char;width:20\" fixed=\"1\">Brand&#10;Code</th>\n" +
            "            <th name=\"bic_zpaksize_sap_s_0material_attr\" type=\"a\" format=\"type:char;width:20\" fixed=\"1\">Pack/Size</th>\n" +
            "            <th name=\"itm_catch_wgt_ind\" type=\"a\" format=\"type:char;width:10\" fixed=\"1\">Item Catch&#10;Weight&#10;Indicator</th>\n" +
            "            <th name=\"case_sold_qty\" type=\"i\" format=\"type:nocommas;width:10\" fixed=\"1\">Full &#10;Cases&#10;Sold</th>\n" +
            "            <th name=\"caseequiv\" type=\"f\" format=\"type:num;width:13;dec:2\" fixed=\"0\">Sum&#10;Case&#10;Equivalents&#10;Sold</th>\n" +
            "            <th name=\"purchasedol\" type=\"f\" format=\"type:num;width:15;dec:2\" fixed=\"0\">Sum&#10;Net&#10;Sales $</th>\n" +
            "        </cols>\n" +
            "        <data>\n" +
            "            <tr>\n" +
            "                <td>20130801</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>4009189</td>\n" +
            "                <td>PEANUT BUTTER CREAMY</td>\n" +
            "                <td>SYS CLS</td>\n" +
            "                <td>6/5LB</td>\n" +
            "                <td>N</td>\n" +
            "                <td>0</td>\n" +
            "                <td>12</td>\n" +
            "                <td>685.76</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20130801</td>\n" +
            "                <td>6</td>\n" +
            "                <td>FROZEN              </td>\n" +
            "                <td>1149602</td>\n" +
            "                <td>BREAD CIABATTA SOFT 4X4 SLI</td>\n" +
            "                <td>BURRY</td>\n" +
            "                <td>64/3.5OZ</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>61</td>\n" +
            "                <td>1362.13</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20130801</td>\n" +
            "                <td>2</td>\n" +
            "                <td>DAIRY PRODUCTS      </td>\n" +
            "                <td>6842938</td>\n" +
            "                <td>YOGURT FROZEN NON FAT FRCH VAN</td>\n" +
            "                <td>EDYDREY</td>\n" +
            "                <td>6/.5GAL</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>58</td>\n" +
            "                <td>1814.1</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20130801</td>\n" +
            "                <td>6</td>\n" +
            "                <td>FROZEN              </td>\n" +
            "                <td>2194009</td>\n" +
            "                <td>BLUEBERRY IQF</td>\n" +
            "                <td>PACKER</td>\n" +
            "                <td>1/30LB</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>52</td>\n" +
            "                <td>2857.48</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20130801</td>\n" +
            "                <td>5</td>\n" +
            "                <td>POULTRY             </td>\n" +
            "                <td>2136956</td>\n" +
            "                <td>CHICKEN BRST BL STRIP 3/8&quot; NAT</td>\n" +
            "                <td>TRPSMTH</td>\n" +
            "                <td>4/5LB</td>\n" +
            "                <td>N</td>\n" +
            "                <td>2</td>\n" +
            "                <td>94</td>\n" +
            "                <td>5396.7</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20130801</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>4106498</td>\n" +
            "                <td>PINEAPPLE TIDBIT JCE FCY</td>\n" +
            "                <td>DOLE</td>\n" +
            "                <td>6/#10</td>\n" +
            "                <td>N</td>\n" +
            "                <td>2</td>\n" +
            "                <td>60</td>\n" +
            "                <td>1831.34</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20130801</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>5229729</td>\n" +
            "                <td>SPICE SESAME SEED WHL WHITE</td>\n" +
            "                <td>IMP/MCC</td>\n" +
            "                <td>6/18OZ</td>\n" +
            "                <td>N</td>\n" +
            "                <td>0</td>\n" +
            "                <td>1.66666666666667</td>\n" +
            "                <td>63.95</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20130801</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>5716602</td>\n" +
            "                <td>WRAP TORTILLA FLOUR 12</td>\n" +
            "                <td>MISSION</td>\n" +
            "                <td>8/12CT</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>45</td>\n" +
            "                <td>965.79</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20130801</td>\n" +
            "                <td>6</td>\n" +
            "                <td>FROZEN              </td>\n" +
            "                <td>5574975</td>\n" +
            "                <td>MANGO CHUNK IQF</td>\n" +
            "                <td>PACKER</td>\n" +
            "                <td>1/20LB</td>\n" +
            "                <td>N</td>\n" +
            "                <td>2</td>\n" +
            "                <td>48</td>\n" +
            "                <td>1152.18</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20130801</td>\n" +
            "                <td>2</td>\n" +
            "                <td>DAIRY PRODUCTS      </td>\n" +
            "                <td>8256323</td>\n" +
            "                <td>CHEESE SWISS SLICE .75 OZ</td>\n" +
            "                <td>BBRLIMP</td>\n" +
            "                <td>8/1.5LB</td>\n" +
            "                <td>N</td>\n" +
            "                <td>0</td>\n" +
            "                <td>1.75</td>\n" +
            "                <td>88.34</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20130801</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>6743530</td>\n" +
            "                <td>CHIP POTATO SALT &amp; VNGR KETTLE</td>\n" +
            "                <td>MSVICKI</td>\n" +
            "                <td>64/1.375Z</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>20</td>\n" +
            "                <td>564.8</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20130801</td>\n" +
            "                <td>8</td>\n" +
            "                <td>PAPER &amp; DISP        </td>\n" +
            "                <td>5324050</td>\n" +
            "                <td>CUP FOAM 24OZ CUSTOM TRPSMTH</td>\n" +
            "                <td>TRPSMTH</td>\n" +
            "                <td>20/25CT</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>26</td>\n" +
            "                <td>1078.88</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20130801</td>\n" +
            "                <td>6</td>\n" +
            "                <td>FROZEN              </td>\n" +
            "                <td>1024439</td>\n" +
            "                <td>STRAWBERRY WHL IQF</td>\n" +
            "                <td>PACKER</td>\n" +
            "                <td>1/30LB</td>\n" +
            "                <td>N</td>\n" +
            "                <td>3</td>\n" +
            "                <td>51</td>\n" +
            "                <td>1827.36</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20130801</td>\n" +
            "                <td>8</td>\n" +
            "                <td>PAPER &amp; DISP        </td>\n" +
            "                <td>8348260</td>\n" +
            "                <td>GLOVE POLY CAST DISP MED</td>\n" +
            "                <td>SYS CLS</td>\n" +
            "                <td>4/100CT</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>8</td>\n" +
            "                <td>98.46</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20130801</td>\n" +
            "                <td>11</td>\n" +
            "                <td>PRODUCE             </td>\n" +
            "                <td>6517627</td>\n" +
            "                <td>CARROT SHRD 1/8</td>\n" +
            "                <td>SYS IMP</td>\n" +
            "                <td>1/5LB</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>12</td>\n" +
            "                <td>125.54</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20130801</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>8072795</td>\n" +
            "                <td>WRAP TORTILLA GRLC HERB 12&quot;</td>\n" +
            "                <td>BBRLCLS</td>\n" +
            "                <td>6/12CT</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>50</td>\n" +
            "                <td>1044.46</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20130801</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>0369183</td>\n" +
            "                <td>NUT ALMOND BLNCHD SLI</td>\n" +
            "                <td>BLUEDIA</td>\n" +
            "                <td>4/2LB</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>1</td>\n" +
            "                <td>57.27</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20130801</td>\n" +
            "                <td>3</td>\n" +
            "                <td>MEATS               </td>\n" +
            "                <td>2536555</td>\n" +
            "                <td>BACON PRECOOKED REGULAR SLICE</td>\n" +
            "                <td>SYS CLS</td>\n" +
            "                <td>2/150CT</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>48</td>\n" +
            "                <td>1686.75</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20130801</td>\n" +
            "                <td>5</td>\n" +
            "                <td>POULTRY             </td>\n" +
            "                <td>7024187</td>\n" +
            "                <td>TURKEY BREAST SLI FRSH</td>\n" +
            "                <td>DIETZ&amp;W</td>\n" +
            "                <td>6/2#AVG</td>\n" +
            "                <td>Y</td>\n" +
            "                <td>1</td>\n" +
            "                <td>47</td>\n" +
            "                <td>2414.66</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20130801</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>5358181</td>\n" +
            "                <td>SUGAR GRANULATED GDN RAW CANE</td>\n" +
            "                <td>FLORCRY</td>\n" +
            "                <td>1/50LB</td>\n" +
            "                <td>N</td>\n" +
            "                <td>2</td>\n" +
            "                <td>88</td>\n" +
            "                <td>3322.88</td>\n" +
            "            </tr>\n" +
            "        </data>\n" +
            "    </table>\n" +
            "    <nrows>1204720</nrows>\n" +
            "</out>";

    public static String TOP5_CATEGORY = "<out>\n" +
            "    <rc>0</rc>\n" +
            "    <msg>querydata successful</msg>\n" +
            "    <table>\n" +
            "        <totals>1</totals>\n" +
            "        <cols>\n" +
            "            <th name=\"date_name\" type=\"a\" fixed=\"1\">date_name</th>\n" +
            "            <th name=\"m0\" type=\"f\" format=\"type:currency\" fixed=\"0\">Category&#10;Description=PRODUCE                                           &#10;col_sort_order=1</th>\n" +
            "            <th name=\"m1\" type=\"f\" format=\"type:currency\" fixed=\"0\">Category&#10;Description=PAPER &amp; DISP                                      &#10;col_sort_order=2</th>\n" +
            "            <th name=\"m2\" type=\"f\" format=\"type:currency\" fixed=\"0\">Category&#10;Description=POULTRY                                           &#10;col_sort_order=3</th>\n" +
            "            <th name=\"m3\" type=\"f\" format=\"type:currency\" fixed=\"0\">Category&#10;Description=CANNED AND DRY                                    &#10;col_sort_order=4</th>\n" +
            "            <th name=\"m4\" type=\"f\" format=\"type:currency\" fixed=\"0\">Category&#10;Description=FROZEN                                            &#10;col_sort_order=5</th>\n" +
            "        </cols>\n" +
            "        <data>\n" +
            "            <tr>\n" +
            "                <td>2013-2013</td>\n" +
            "                <td>1015442.22999999</td>\n" +
            "                <td>1929886.72</td>\n" +
            "                <td>1846481.23</td>\n" +
            "                <td>3857411.89</td>\n" +
            "                <td>4627753.34999997</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>2014-2014</td>\n" +
            "                <td>2976519.67999997</td>\n" +
            "                <td>5427746.03000009</td>\n" +
            "                <td>5408840.4199999</td>\n" +
            "                <td>11421210.6200004</td>\n" +
            "                <td>13580299.0999998</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>2015-2015</td>\n" +
            "                <td>3852164.95999998</td>\n" +
            "                <td>6863033.95000004</td>\n" +
            "                <td>7651588.58999997</td>\n" +
            "                <td>14054759.0900005</td>\n" +
            "                <td>19820526.1199998</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>2016-2016</td>\n" +
            "                <td>4875552.67000003</td>\n" +
            "                <td>8572037.19999998</td>\n" +
            "                <td>9493436.77999997</td>\n" +
            "                <td>18612746.4500006</td>\n" +
            "                <td>23633142.1500004</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>2017-2017</td>\n" +
            "                <td>272721.15</td>\n" +
            "                <td>489308.01</td>\n" +
            "                <td>570418.07</td>\n" +
            "                <td>1057714.85</td>\n" +
            "                <td>1209360.56</td>\n" +
            "            </tr>\n" +
            "        </data>\n" +
            "    </table>\n" +
            "    <nrows>5</nrows>\n" +
            "</out>";
}
