package sysco.cake.mock.api.service;

/**
 * Created by supun on 1/20/17.
 */
public final class XmlWeeklyResponses {

    private XmlWeeklyResponses(){}

    public static String TOTAL_DELIVERY = "<out>\n" +
            "    <rc>0</rc>\n" +
            "    <msg>querydata successful</msg>\n" +
            "    <table>\n" +
            "        <totals>1</totals>\n" +
            "        <cols>\n" +
            "            <th name=\"col_name\" type=\"a\" fixed=\"1\">col_name</th>\n" +
            "            <th name=\"value\" type=\"i\" format=\"type:num\" fixed=\"0\">Ucnt&#10;Route&#10;Number</th>\n" +
            "        </cols>\n" +
            "        <data>\n" +
            "            <tr>\n" +
            "                <td>Previos_range</td>\n" +
            "                <td>733</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>Current_range</td>\n" +
            "                <td>701</td>\n" +
            "            </tr>\n" +
            "        </data>\n" +
            "    </table>\n" +
            "    <nrows>2</nrows>\n" +
            "</out>";

    public static String TOTAL_INVOICE = "<out>\n" +
            "    <rc>0</rc>\n" +
            "    <msg>querydata successful</msg>\n" +
            "    <table>\n" +
            "        <totals>1</totals>\n" +
            "        <cols>\n" +
            "            <th name=\"col_name\" type=\"a\" fixed=\"1\">col_name</th>\n" +
            "            <th name=\"value\" type=\"i\" format=\"type:num\" fixed=\"0\">Ucnt&#10;Obligation&#10;Number</th>\n" +
            "        </cols>\n" +
            "        <data>\n" +
            "            <tr>\n" +
            "                <td>Previos_range</td>\n" +
            "                <td>2185</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>Current_range</td>\n" +
            "                <td>2074</td>\n" +
            "            </tr>\n" +
            "        </data>\n" +
            "    </table>\n" +
            "    <nrows>2</nrows>\n" +
            "</out>";

    public static String TOTAL_PURCHASE = "<out>\n" +
            "    <rc>0</rc>\n" +
            "    <msg>querydata successful</msg>\n" +
            "    <table>\n" +
            "        <totals>1</totals>\n" +
            "        <cols>\n" +
            "            <th name=\"col_name\" type=\"a\" fixed=\"1\">col_name</th>\n" +
            "            <th name=\"value\" type=\"f\" format=\"type:currency\" fixed=\"0\">Sum&#10;Net&#10;Sales $</th>\n" +
            "        </cols>\n" +
            "        <data>\n" +
            "            <tr>\n" +
            "                <td>Previos_range</td>\n" +
            "                <td>1367573.15</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>Current_range</td>\n" +
            "                <td>1657848.72</td>\n" +
            "            </tr>\n" +
            "        </data>\n" +
            "    </table>\n" +
            "    <nrows>2</nrows>\n" +
            "</out>";

    public static String TOP_CATEGORIES = "<out>\n" +
            "    <rc>0</rc>\n" +
            "    <msg>querydata successful</msg>\n" +
            "    <table>\n" +
            "        <totals>1</totals>\n" +
            "        <cols>\n" +
            "            <th name=\"netSalesPercent\" type=\"f\" format=\"type:num;width:5;dec:0\" fixed=\"0\">PGsum&#10;Net&#10;Sales $</th>\n" +
            "            <th name=\"netSales\" type=\"f\" format=\"type:num;width:13;dec:2\" fixed=\"0\">Sum&#10;Net&#10;Sales $</th>\n" +
            "            <th name=\"catName\" type=\"a\" format=\"type:char;width:50\" fixed=\"0\">catName</th>\n" +
            "            <th name=\"catId\" type=\"i\" format=\"type:nocommas;width:10\" fixed=\"0\">catId</th>\n" +
            "        </cols>\n" +
            "        <data>\n" +
            "            <tr>\n" +
            "                <td>28.7393868014105</td>\n" +
            "                <td>464230.370000001</td>\n" +
            "                <td>FROZEN              </td>\n" +
            "                <td>6</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>25.3627000523113</td>\n" +
            "                <td>409686.39</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>7</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>13.5473949057092</td>\n" +
            "                <td>218832.51</td>\n" +
            "                <td>POULTRY             </td>\n" +
            "                <td>5</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>11.6818688923009</td>\n" +
            "                <td>188698.47</td>\n" +
            "                <td>PAPER &amp; DISP        </td>\n" +
            "                <td>8</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>6.43679971325391</td>\n" +
            "                <td>103974.31</td>\n" +
            "                <td>PRODUCE             </td>\n" +
            "                <td>11</td>\n" +
            "            </tr>\n" +
            "        </data>\n" +
            "    </table>\n" +
            "    <nrows>5</nrows>\n" +
            "</out>";

    public static String PURCHASE_DATA = "<out>\n" +
            "    <rc>0</rc>\n" +
            "    <msg>querydata successful</msg>\n" +
            "    <table>\n" +
            "        <totals>1</totals>\n" +
            "        <cols>\n" +
            "            <th name=\"oblig_dt\" type=\"i\" format=\"type:ansidate\" fixed=\"1\">Obligation&#10;Invoice&#10;Date</th>\n" +
            "            <th name=\"catgy_id\" type=\"i\" format=\"type:nocommas;width:10\" fixed=\"1\">Category ID</th>\n" +
            "            <th name=\"ec_category_description\" type=\"a\" format=\"type:char;width:50\" fixed=\"1\">Category&#10;Description</th>\n" +
            "            <th name=\"itm_nbr\" type=\"a\" format=\"type:char;width:20\" fixed=\"1\">Item&#10;Number</th>\n" +
            "            <th name=\"itm_desc_sus_itm\" type=\"a\" format=\"type:char;width:20\" fixed=\"1\">Item&#10;Description</th>\n" +
            "            <th name=\"brnd_cd_sus_itm\" type=\"a\" format=\"type:char;width:20\" fixed=\"1\">Brand&#10;Code</th>\n" +
            "            <th name=\"bic_zpaksize_sap_s_0material_attr\" type=\"a\" format=\"type:char;width:20\" fixed=\"1\">Pack/Size</th>\n" +
            "            <th name=\"itm_catch_wgt_ind\" type=\"a\" format=\"type:char;width:10\" fixed=\"1\">Item Catch&#10;Weight&#10;Indicator</th>\n" +
            "            <th name=\"case_sold_qty\" type=\"i\" format=\"type:nocommas;width:10\" fixed=\"1\">Full &#10;Cases&#10;Sold</th>\n" +
            "            <th name=\"caseequiv\" type=\"f\" format=\"type:num;width:10;dec:2\" fixed=\"0\">Sum&#10;Case&#10;Equivalents&#10;Sold</th>\n" +
            "            <th name=\"purchasedol\" type=\"f\" format=\"type:num;width:13;dec:2\" fixed=\"0\">Sum&#10;Net&#10;Sales $</th>\n" +
            "        </cols>\n" +
            "        <data>\n" +
            "            <tr>\n" +
            "                <td>20170116</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>9477498</td>\n" +
            "                <td>ALLOWANCE FOR DROP SIZE</td>\n" +
            "                <td>NONPROD</td>\n" +
            "                <td></td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>93</td>\n" +
            "                <td>-1703.08</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170116</td>\n" +
            "                <td>6</td>\n" +
            "                <td>FROZEN              </td>\n" +
            "                <td>3653880</td>\n" +
            "                <td>RICE BROWN FARRO QUINOA BLND</td>\n" +
            "                <td>SAVOR</td>\n" +
            "                <td>6/4LB</td>\n" +
            "                <td>N</td>\n" +
            "                <td>2</td>\n" +
            "                <td>2</td>\n" +
            "                <td>101.54</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170116</td>\n" +
            "                <td>5</td>\n" +
            "                <td>POULTRY             </td>\n" +
            "                <td>7024187</td>\n" +
            "                <td>TURKEY BREAST SLI FRSH</td>\n" +
            "                <td>DIETZ&amp;W</td>\n" +
            "                <td>6/2#AVG</td>\n" +
            "                <td>Y</td>\n" +
            "                <td>2</td>\n" +
            "                <td>94</td>\n" +
            "                <td>5834.51</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170116</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>5339664</td>\n" +
            "                <td>JUICE CONC KIWI</td>\n" +
            "                <td>THREE V</td>\n" +
            "                <td>4/1GAL</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>57</td>\n" +
            "                <td>3356.63</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170116</td>\n" +
            "                <td>2</td>\n" +
            "                <td>DAIRY PRODUCTS      </td>\n" +
            "                <td>2336865</td>\n" +
            "                <td>CHEESE MOZZARELLA SHRED LMPS</td>\n" +
            "                <td>AREZIMP</td>\n" +
            "                <td>6/5LB</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>4</td>\n" +
            "                <td>281.08</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170116</td>\n" +
            "                <td>6</td>\n" +
            "                <td>FROZEN              </td>\n" +
            "                <td>3334220</td>\n" +
            "                <td>WRAP TORTILLA HERB GRLC 12</td>\n" +
            "                <td>TRPSMTH</td>\n" +
            "                <td>6/12CT</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>105</td>\n" +
            "                <td>2075.14</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170116</td>\n" +
            "                <td>8</td>\n" +
            "                <td>PAPER &amp; DISP        </td>\n" +
            "                <td>7790795</td>\n" +
            "                <td>LID PLAS CLR F/1.5-2.5OZ PRTN</td>\n" +
            "                <td>SYS IMP</td>\n" +
            "                <td>24/100CT</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>5</td>\n" +
            "                <td>124.64</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170116</td>\n" +
            "                <td>6</td>\n" +
            "                <td>FROZEN              </td>\n" +
            "                <td>3665750</td>\n" +
            "                <td>FRUIT MANGO CHUNK IQF</td>\n" +
            "                <td>TRPSMTH</td>\n" +
            "                <td>1/20LB</td>\n" +
            "                <td>N</td>\n" +
            "                <td>3</td>\n" +
            "                <td>111</td>\n" +
            "                <td>3402.93</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170116</td>\n" +
            "                <td>8</td>\n" +
            "                <td>PAPER &amp; DISP        </td>\n" +
            "                <td>7012880</td>\n" +
            "                <td>CUP FOAM TRP SMTH FRESH 24X24</td>\n" +
            "                <td>TRPSMTH</td>\n" +
            "                <td>500/24OZ</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>61</td>\n" +
            "                <td>2423.01</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170116</td>\n" +
            "                <td>6</td>\n" +
            "                <td>FROZEN              </td>\n" +
            "                <td>7222843</td>\n" +
            "                <td>BREAD PANINI PREGRILLED 7&quot;</td>\n" +
            "                <td>ALLADN</td>\n" +
            "                <td>12/10EA</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>106</td>\n" +
            "                <td>3725.87</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170116</td>\n" +
            "                <td>8</td>\n" +
            "                <td>PAPER &amp; DISP        </td>\n" +
            "                <td>7319056</td>\n" +
            "                <td>STRAW PLAS WRPD GNT 10.25</td>\n" +
            "                <td>DIXIE</td>\n" +
            "                <td>4/300CT</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>82</td>\n" +
            "                <td>1392.32</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170116</td>\n" +
            "                <td>2</td>\n" +
            "                <td>DAIRY PRODUCTS      </td>\n" +
            "                <td>2431847</td>\n" +
            "                <td>YOGURT FROZEN ORIG</td>\n" +
            "                <td>TRPSMTH</td>\n" +
            "                <td>4/1GAL</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>127</td>\n" +
            "                <td>5107.81</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170116</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>4106498</td>\n" +
            "                <td>PINEAPPLE TIDBIT JCE FCY</td>\n" +
            "                <td>DOLE</td>\n" +
            "                <td>6/#10</td>\n" +
            "                <td>N</td>\n" +
            "                <td>2</td>\n" +
            "                <td>112</td>\n" +
            "                <td>3587.64</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170116</td>\n" +
            "                <td>8</td>\n" +
            "                <td>PAPER &amp; DISP        </td>\n" +
            "                <td>7793829</td>\n" +
            "                <td>CUP PLAS PRTN TRANS 4OZ</td>\n" +
            "                <td>SYS REL</td>\n" +
            "                <td>12/200CT</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>1</td>\n" +
            "                <td>43.13</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170116</td>\n" +
            "                <td>11</td>\n" +
            "                <td>PRODUCE             </td>\n" +
            "                <td>5775507</td>\n" +
            "                <td>CARROT MATCHSTICK SHRED 1/8</td>\n" +
            "                <td>SYS NAT</td>\n" +
            "                <td>2/5LB</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>4</td>\n" +
            "                <td>50.97</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170116</td>\n" +
            "                <td>1</td>\n" +
            "                <td>HLTHCAR/HOSPLTY     </td>\n" +
            "                <td>6969218</td>\n" +
            "                <td>SUPPLEMENT MIX WHEY BLEND</td>\n" +
            "                <td>TOTALNT</td>\n" +
            "                <td>1/25LB</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>78</td>\n" +
            "                <td>9487.99</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170116</td>\n" +
            "                <td>6</td>\n" +
            "                <td>FROZEN              </td>\n" +
            "                <td>3333855</td>\n" +
            "                <td>TORTILLA FLOUR 12 PRSSD</td>\n" +
            "                <td>TRPSMTH</td>\n" +
            "                <td>6/12CT</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>70</td>\n" +
            "                <td>1134.05</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170116</td>\n" +
            "                <td>8</td>\n" +
            "                <td>PAPER &amp; DISP        </td>\n" +
            "                <td>5948948</td>\n" +
            "                <td>TRAY PLAS BLACK PET ROUND</td>\n" +
            "                <td>SYSCO</td>\n" +
            "                <td>36/18IN</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>1</td>\n" +
            "                <td>57.97</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170116</td>\n" +
            "                <td>6</td>\n" +
            "                <td>FROZEN              </td>\n" +
            "                <td>2194009</td>\n" +
            "                <td>BLUEBERRY IQF</td>\n" +
            "                <td>PACKER</td>\n" +
            "                <td>1/30LB</td>\n" +
            "                <td>N</td>\n" +
            "                <td>2</td>\n" +
            "                <td>108</td>\n" +
            "                <td>4806</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170116</td>\n" +
            "                <td>2</td>\n" +
            "                <td>DAIRY PRODUCTS      </td>\n" +
            "                <td>3984396</td>\n" +
            "                <td>EGG PATTY IQF 1.5 OZ</td>\n" +
            "                <td>WHLFCLS</td>\n" +
            "                <td>160/1.5OZ</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>23</td>\n" +
            "                <td>765.87</td>\n" +
            "            </tr>\n" +
            "        </data>\n" +
            "    </table>\n" +
            "    <nrows>5811</nrows>\n" +
            "</out>";

    public static String TOP5_CATEGORY = "<out>\n" +
            "    <rc>0</rc>\n" +
            "    <msg>querydata successful</msg>\n" +
            "    <table>\n" +
            "        <totals>1</totals>\n" +
            "        <cols>\n" +
            "            <th name=\"date_name\" type=\"a\" fixed=\"1\">date_name</th>\n" +
            "            <th name=\"m0\" type=\"f\" format=\"type:currency\" fixed=\"0\">Category&#10;Description=PRODUCE                                           &#10;col_sort_order=1</th>\n" +
            "            <th name=\"m1\" type=\"f\" format=\"type:currency\" fixed=\"0\">Category&#10;Description=PAPER &amp; DISP                                      &#10;col_sort_order=2</th>\n" +
            "            <th name=\"m2\" type=\"f\" format=\"type:currency\" fixed=\"0\">Category&#10;Description=POULTRY                                           &#10;col_sort_order=3</th>\n" +
            "            <th name=\"m3\" type=\"f\" format=\"type:currency\" fixed=\"0\">Category&#10;Description=CANNED AND DRY                                    &#10;col_sort_order=4</th>\n" +
            "            <th name=\"m4\" type=\"f\" format=\"type:currency\" fixed=\"0\">Category&#10;Description=FROZEN                                            &#10;col_sort_order=5</th>\n" +
            "        </cols>\n" +
            "        <data>\n" +
            "            <tr>\n" +
            "                <td>2017 January-Mon 16</td>\n" +
            "                <td>27278.82</td>\n" +
            "                <td>45550.52</td>\n" +
            "                <td>57334.44</td>\n" +
            "                <td>104996.43</td>\n" +
            "                <td>118532.95</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>2017 January-Tue 17</td>\n" +
            "                <td>21519.84</td>\n" +
            "                <td>44499.89</td>\n" +
            "                <td>51869.14</td>\n" +
            "                <td>96260.58</td>\n" +
            "                <td>116434.9</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>2017 January-Wed 18</td>\n" +
            "                <td>7357.7</td>\n" +
            "                <td>14823.76</td>\n" +
            "                <td>17745.86</td>\n" +
            "                <td>31880.43</td>\n" +
            "                <td>35566.76</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>2017 January-Thu 19</td>\n" +
            "                <td>23336.45</td>\n" +
            "                <td>43592.51</td>\n" +
            "                <td>46600.71</td>\n" +
            "                <td>91170.56</td>\n" +
            "                <td>101159.22</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>2017 January-Fri 20</td>\n" +
            "                <td>24481.5</td>\n" +
            "                <td>40231.79</td>\n" +
            "                <td>45282.36</td>\n" +
            "                <td>85378.39</td>\n" +
            "                <td>92536.54</td>\n" +
            "            </tr>\n" +
            "        </data>\n" +
            "    </table>\n" +
            "    <nrows>5</nrows>\n" +
            "</out>";
}
