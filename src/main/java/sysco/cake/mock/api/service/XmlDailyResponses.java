package sysco.cake.mock.api.service;

/**
 * Created by supun on 1/20/17.
 */
public final class XmlDailyResponses {

    private XmlDailyResponses(){}

    public static String TOTAL_DELIVERY = "<out>\n" +
            "    <rc>0</rc>\n" +
            "    <msg>querydata successful</msg>\n" +
            "    <table>\n" +
            "        <totals>1</totals>\n" +
            "        <cols>\n" +
            "            <th name=\"col_name\" type=\"a\" fixed=\"1\">col_name</th>\n" +
            "            <th name=\"value\" type=\"i\" format=\"type:num\" fixed=\"0\">Ucnt&#10;Route&#10;Number</th>\n" +
            "        </cols>\n" +
            "        <data>\n" +
            "            <tr>\n" +
            "                <td>Previos_range</td>\n" +
            "                <td>164</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>Current_range</td>\n" +
            "                <td>190</td>\n" +
            "            </tr>\n" +
            "        </data>\n" +
            "    </table>\n" +
            "    <nrows>2</nrows>\n" +
            "</out>";

    public static String TOTAL_INVOICE = "<out>\n" +
            "    <rc>0</rc>\n" +
            "    <msg>querydata successful</msg>\n" +
            "    <table>\n" +
            "        <totals>1</totals>\n" +
            "        <cols>\n" +
            "            <th name=\"col_name\" type=\"a\" fixed=\"1\">col_name</th>\n" +
            "            <th name=\"value\" type=\"i\" format=\"type:num\" fixed=\"0\">Ucnt&#10;Obligation&#10;Number</th>\n" +
            "        </cols>\n" +
            "        <data>\n" +
            "            <tr>\n" +
            "                <td>Previos_range</td>\n" +
            "                <td>259</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>Current_range</td>\n" +
            "                <td>312</td>\n" +
            "            </tr>\n" +
            "        </data>\n" +
            "    </table>\n" +
            "    <nrows>2</nrows>\n" +
            "</out>";

    public static String TOTAL_PURCHASE = "<out>\n" +
            "    <rc>0</rc>\n" +
            "    <msg>querydata successful</msg>\n" +
            "    <table>\n" +
            "        <totals>1</totals>\n" +
            "        <cols>\n" +
            "            <th name=\"col_name\" type=\"a\" fixed=\"1\">col_name</th>\n" +
            "            <th name=\"value\" type=\"f\" format=\"type:currency\" fixed=\"0\">Sum&#10;Net&#10;Sales $</th>\n" +
            "        </cols>\n" +
            "        <data>\n" +
            "            <tr>\n" +
            "                <td>Previos_range</td>\n" +
            "                <td>364488.6</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>Current_range</td>\n" +
            "                <td>349650.69</td>\n" +
            "            </tr>\n" +
            "        </data>\n" +
            "    </table>\n" +
            "    <nrows>2</nrows>\n" +
            "</out>";

    public static String TOP_CATEGORIES = "<out>\n" +
            "    <rc>0</rc>\n" +
            "    <msg>querydata successful</msg>\n" +
            "    <table>\n" +
            "        <totals>1</totals>\n" +
            "        <cols>\n" +
            "            <th name=\"netSalesPercent\" type=\"f\" format=\"type:num;width:5;dec:0\" fixed=\"0\">PGsum&#10;Net&#10;Sales $</th>\n" +
            "            <th name=\"netSales\" type=\"f\" format=\"type:num;width:11;dec:2\" fixed=\"0\">Sum&#10;Net&#10;Sales $</th>\n" +
            "            <th name=\"catName\" type=\"a\" format=\"type:char;width:50\" fixed=\"0\">catName</th>\n" +
            "            <th name=\"catId\" type=\"i\" format=\"type:nocommas;width:10\" fixed=\"0\">catId</th>\n" +
            "        </cols>\n" +
            "        <data>\n" +
            "            <tr>\n" +
            "                <td>27.3514576176362</td>\n" +
            "                <td>92536.54</td>\n" +
            "                <td>FROZEN              </td>\n" +
            "                <td>6</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>25.2356897669506</td>\n" +
            "                <td>85378.39</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>7</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>13.3843187822513</td>\n" +
            "                <td>45282.36</td>\n" +
            "                <td>POULTRY             </td>\n" +
            "                <td>5</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>11.8914982024035</td>\n" +
            "                <td>40231.79</td>\n" +
            "                <td>PAPER &amp; DISP        </td>\n" +
            "                <td>8</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>7.2361113746652</td>\n" +
            "                <td>24481.5</td>\n" +
            "                <td>PRODUCE             </td>\n" +
            "                <td>11</td>\n" +
            "            </tr>\n" +
            "        </data>\n" +
            "    </table>\n" +
            "    <nrows>5</nrows>\n" +
            "</out>";

    public static String PURCHASE_DATA = "<out>\n" +
            "    <rc>0</rc>\n" +
            "    <msg>querydata successful</msg>\n" +
            "    <table>\n" +
            "        <totals>1</totals>\n" +
            "        <cols>\n" +
            "            <th name=\"oblig_dt\" type=\"i\" format=\"type:ansidate\" fixed=\"1\">Obligation&#10;Invoice&#10;Date</th>\n" +
            "            <th name=\"catgy_id\" type=\"i\" format=\"type:nocommas;width:10\" fixed=\"1\">Category ID</th>\n" +
            "            <th name=\"ec_category_description\" type=\"a\" format=\"type:char;width:50\" fixed=\"1\">Category&#10;Description</th>\n" +
            "            <th name=\"itm_nbr\" type=\"a\" format=\"type:char;width:20\" fixed=\"1\">Item&#10;Number</th>\n" +
            "            <th name=\"itm_desc_sus_itm\" type=\"a\" format=\"type:char;width:20\" fixed=\"1\">Item&#10;Description</th>\n" +
            "            <th name=\"brnd_cd_sus_itm\" type=\"a\" format=\"type:char;width:20\" fixed=\"1\">Brand&#10;Code</th>\n" +
            "            <th name=\"bic_zpaksize_sap_s_0material_attr\" type=\"a\" format=\"type:char;width:20\" fixed=\"1\">Pack/Size</th>\n" +
            "            <th name=\"itm_catch_wgt_ind\" type=\"a\" format=\"type:char;width:10\" fixed=\"1\">Item Catch&#10;Weight&#10;Indicator</th>\n" +
            "            <th name=\"case_sold_qty\" type=\"i\" format=\"type:nocommas;width:10\" fixed=\"1\">Full &#10;Cases&#10;Sold</th>\n" +
            "            <th name=\"caseequiv\" type=\"f\" format=\"type:num;width:9;dec:2\" fixed=\"0\">Sum&#10;Case&#10;Equivalents&#10;Sold</th>\n" +
            "            <th name=\"purchasedol\" type=\"f\" format=\"type:num;width:11;dec:2\" fixed=\"0\">Sum&#10;Net&#10;Sales $</th>\n" +
            "        </cols>\n" +
            "        <data>\n" +
            "            <tr>\n" +
            "                <td>20170120</td>\n" +
            "                <td>3</td>\n" +
            "                <td>MEATS               </td>\n" +
            "                <td>0090811</td>\n" +
            "                <td>BACON LAYFLAT PRCK</td>\n" +
            "                <td>FARMLND</td>\n" +
            "                <td>2/150SLI</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>101</td>\n" +
            "                <td>3056.67</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170120</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>2182388</td>\n" +
            "                <td>PEACH SLICED IRREG IN EXTRA LS</td>\n" +
            "                <td>SYS REL</td>\n" +
            "                <td>6/#10</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>9</td>\n" +
            "                <td>354.44</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170120</td>\n" +
            "                <td>8</td>\n" +
            "                <td>PAPER &amp; DISP        </td>\n" +
            "                <td>7012880</td>\n" +
            "                <td>CUP FOAM TRP SMTH FRESH 24X24</td>\n" +
            "                <td>TRPSMTH</td>\n" +
            "                <td>500/24OZ</td>\n" +
            "                <td>N</td>\n" +
            "                <td>2</td>\n" +
            "                <td>144</td>\n" +
            "                <td>5744.3</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170120</td>\n" +
            "                <td>9</td>\n" +
            "                <td>CHEMICAL/JANTRL     </td>\n" +
            "                <td>7681337</td>\n" +
            "                <td>DETERGENT POT/PAN LIQ BLU DISP</td>\n" +
            "                <td>KEYSTON</td>\n" +
            "                <td>2/2L</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>4</td>\n" +
            "                <td>114.91</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170120</td>\n" +
            "                <td>6</td>\n" +
            "                <td>FROZEN              </td>\n" +
            "                <td>1024439</td>\n" +
            "                <td>STRAWBERRY WHL IQF</td>\n" +
            "                <td>PACKER</td>\n" +
            "                <td>1/30LB</td>\n" +
            "                <td>N</td>\n" +
            "                <td>2</td>\n" +
            "                <td>100</td>\n" +
            "                <td>3848.5</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170120</td>\n" +
            "                <td>6</td>\n" +
            "                <td>FROZEN              </td>\n" +
            "                <td>1997511</td>\n" +
            "                <td>STRIP CHICKEN-FREE LTY SEASON</td>\n" +
            "                <td>BEYMEAT</td>\n" +
            "                <td>2/5LB</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>13</td>\n" +
            "                <td>634.77</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170120</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>9477498</td>\n" +
            "                <td>ALLOWANCE FOR DROP SIZE</td>\n" +
            "                <td>NONPROD</td>\n" +
            "                <td></td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>79</td>\n" +
            "                <td>-1206.73</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170120</td>\n" +
            "                <td>6</td>\n" +
            "                <td>FROZEN              </td>\n" +
            "                <td>9669151</td>\n" +
            "                <td>DIP HUMMUS TRADITIONAL</td>\n" +
            "                <td>MKZ CLS</td>\n" +
            "                <td>4/.5GAL</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>8</td>\n" +
            "                <td>282.99</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170120</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>5358181</td>\n" +
            "                <td>SUGAR GRANULATED GDN RAW CANE</td>\n" +
            "                <td>FLORCRY</td>\n" +
            "                <td>1/50LB</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>29</td>\n" +
            "                <td>1027.1</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170120</td>\n" +
            "                <td>6</td>\n" +
            "                <td>FROZEN              </td>\n" +
            "                <td>2431854</td>\n" +
            "                <td>JUICE ORANGE CONC FRZN</td>\n" +
            "                <td>TRPSMTH</td>\n" +
            "                <td>9/64OZ</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>78</td>\n" +
            "                <td>3858.41</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170120</td>\n" +
            "                <td>2</td>\n" +
            "                <td>DAIRY PRODUCTS      </td>\n" +
            "                <td>2431847</td>\n" +
            "                <td>YOGURT FROZEN ORIG</td>\n" +
            "                <td>TRPSMTH</td>\n" +
            "                <td>4/1GAL</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>97</td>\n" +
            "                <td>3898.96</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170120</td>\n" +
            "                <td>2</td>\n" +
            "                <td>DAIRY PRODUCTS      </td>\n" +
            "                <td>3984396</td>\n" +
            "                <td>EGG PATTY IQF 1.5 OZ</td>\n" +
            "                <td>WHLFCLS</td>\n" +
            "                <td>160/1.5OZ</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>20</td>\n" +
            "                <td>671.59</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170120</td>\n" +
            "                <td>8</td>\n" +
            "                <td>PAPER &amp; DISP        </td>\n" +
            "                <td>4358990</td>\n" +
            "                <td>BAG PLAS 6.5X7.5 HI-DEN SANDW</td>\n" +
            "                <td>SYSCO</td>\n" +
            "                <td>1/2000CT</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>7</td>\n" +
            "                <td>208.58</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170120</td>\n" +
            "                <td>11</td>\n" +
            "                <td>PRODUCE             </td>\n" +
            "                <td>1763440</td>\n" +
            "                <td>TOMATO BULK 6X6 FRESH</td>\n" +
            "                <td>SYS IMP</td>\n" +
            "                <td>1/25LB</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>27</td>\n" +
            "                <td>515.94</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170120</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>8256893</td>\n" +
            "                <td>CRANBERRY DRIED</td>\n" +
            "                <td>SYS SUP</td>\n" +
            "                <td>1/5LB</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>1</td>\n" +
            "                <td>20.27</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170120</td>\n" +
            "                <td>8</td>\n" +
            "                <td>PAPER &amp; DISP        </td>\n" +
            "                <td>3086594</td>\n" +
            "                <td>LID PLAS CLR DOME LOW F/ 24 OZ</td>\n" +
            "                <td>M&amp;N</td>\n" +
            "                <td>10/100CT</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>101</td>\n" +
            "                <td>2910.97</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170120</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>4106498</td>\n" +
            "                <td>PINEAPPLE TIDBIT JCE FCY</td>\n" +
            "                <td>DOLE</td>\n" +
            "                <td>6/#10</td>\n" +
            "                <td>N</td>\n" +
            "                <td>2</td>\n" +
            "                <td>102</td>\n" +
            "                <td>3266.6</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170120</td>\n" +
            "                <td>8</td>\n" +
            "                <td>PAPER &amp; DISP        </td>\n" +
            "                <td>5989878</td>\n" +
            "                <td>GLOVE VINYL FDSV PF MED</td>\n" +
            "                <td>SYS CLS</td>\n" +
            "                <td>4/100CT</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>1</td>\n" +
            "                <td>28.3</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170120</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>5386754</td>\n" +
            "                <td>PECAN PCS LG CANDIED</td>\n" +
            "                <td>AZAR</td>\n" +
            "                <td>1/5#</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>12</td>\n" +
            "                <td>614.94</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20170120</td>\n" +
            "                <td>6</td>\n" +
            "                <td>FROZEN              </td>\n" +
            "                <td>0356087</td>\n" +
            "                <td>COOKIE SNICKERDOODLE T&amp;S</td>\n" +
            "                <td>CHRISTI</td>\n" +
            "                <td>84/2.5OZ</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>35</td>\n" +
            "                <td>1479.48</td>\n" +
            "            </tr>\n" +
            "        </data>\n" +
            "    </table>\n" +
            "    <nrows>1293</nrows>\n" +
            "</out>";

    public static String TOP5_CATEGORY = "<out>\n" +
            "    <rc>0</rc>\n" +
            "    <msg>querydata successful</msg>\n" +
            "    <table>\n" +
            "        <totals>1</totals>\n" +
            "        <cols>\n" +
            "            <th name=\"date_name\" type=\"a\" fixed=\"1\">date_name</th>\n" +
            "            <th name=\"m0\" type=\"f\" format=\"type:currency\" fixed=\"0\">Category&#10;Description=PRODUCE                                           &#10;col_sort_order=1</th>\n" +
            "            <th name=\"m1\" type=\"f\" format=\"type:currency\" fixed=\"0\">Category&#10;Description=PAPER &amp; DISP                                      &#10;col_sort_order=2</th>\n" +
            "            <th name=\"m2\" type=\"f\" format=\"type:currency\" fixed=\"0\">Category&#10;Description=POULTRY                                           &#10;col_sort_order=3</th>\n" +
            "            <th name=\"m3\" type=\"f\" format=\"type:currency\" fixed=\"0\">Category&#10;Description=CANNED AND DRY                                    &#10;col_sort_order=4</th>\n" +
            "            <th name=\"m4\" type=\"f\" format=\"type:currency\" fixed=\"0\">Category&#10;Description=FROZEN                                            &#10;col_sort_order=5</th>\n" +
            "        </cols>\n" +
            "        <data>\n" +
            "            <tr>\n" +
            "                <td>2017 January-Fri 20</td>\n" +
            "                <td>24481.5</td>\n" +
            "                <td>40231.79</td>\n" +
            "                <td>45282.36</td>\n" +
            "                <td>85378.39</td>\n" +
            "                <td>92536.54</td>\n" +
            "            </tr>\n" +
            "        </data>\n" +
            "    </table>\n" +
            "    <nrows>1</nrows>\n" +
            "</out>";
}
