package sysco.cake.mock.api.service;

/**
 * Created by supun on 1/20/17.
 */
public final class XmlMonthlyResponses {

    private XmlMonthlyResponses(){}

    public static String TOTAL_DELIVERY = "<out>\n" +
            "    <rc>0</rc>\n" +
            "    <msg>querydata successful</msg>\n" +
            "    <table>\n" +
            "        <totals>1</totals>\n" +
            "        <cols>\n" +
            "            <th name=\"col_name\" type=\"a\" fixed=\"1\">col_name</th>\n" +
            "            <th name=\"value\" type=\"i\" format=\"type:num\" fixed=\"0\">Ucnt&#10;Route&#10;Number</th>\n" +
            "        </cols>\n" +
            "        <data>\n" +
            "            <tr>\n" +
            "                <td>Previos_range</td>\n" +
            "                <td>2885</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>Current_range</td>\n" +
            "                <td>2790</td>\n" +
            "            </tr>\n" +
            "        </data>\n" +
            "    </table>\n" +
            "    <nrows>2</nrows>\n" +
            "</out>";

    public static String TOTAL_INVOICE = "<out>\n" +
            "    <rc>0</rc>\n" +
            "    <msg>querydata successful</msg>\n" +
            "    <table>\n" +
            "        <totals>1</totals>\n" +
            "        <cols>\n" +
            "            <th name=\"col_name\" type=\"a\" fixed=\"1\">col_name</th>\n" +
            "            <th name=\"value\" type=\"i\" format=\"type:num\" fixed=\"0\">Ucnt&#10;Obligation&#10;Number</th>\n" +
            "        </cols>\n" +
            "        <data>\n" +
            "            <tr>\n" +
            "                <td>Previos_range</td>\n" +
            "                <td>48325</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>Current_range</td>\n" +
            "                <td>48213</td>\n" +
            "            </tr>\n" +
            "        </data>\n" +
            "    </table>\n" +
            "    <nrows>2</nrows>\n" +
            "</out>";

    public static String TOTAL_PURCHASE = "<out>\n" +
            "    <rc>0</rc>\n" +
            "    <msg>querydata successful</msg>\n" +
            "    <table>\n" +
            "        <totals>1</totals>\n" +
            "        <cols>\n" +
            "            <th name=\"col_name\" type=\"a\" fixed=\"1\">col_name</th>\n" +
            "            <th name=\"value\" type=\"f\" format=\"type:currency\" fixed=\"0\">Sum&#10;Net&#10;Sales $</th>\n" +
            "        </cols>\n" +
            "        <data>\n" +
            "            <tr>\n" +
            "                <td>Previos_range</td>\n" +
            "                <td>44802873.5599974</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>Current_range</td>\n" +
            "                <td>38517811.4099987</td>\n" +
            "            </tr>\n" +
            "        </data>\n" +
            "    </table>\n" +
            "    <nrows>2</nrows>\n" +
            "</out>";

    public static String TOP_CATEGORIES = "<out>\n" +
            "    <rc>0</rc>\n" +
            "    <msg>querydata successful</msg>\n" +
            "    <table>\n" +
            "        <totals>1</totals>\n" +
            "        <cols>\n" +
            "            <th name=\"netSalesPercent\" type=\"f\" format=\"type:num;width:5;dec:0\" fixed=\"0\">PGsum&#10;Net&#10;Sales $</th>\n" +
            "            <th name=\"netSales\" type=\"f\" format=\"type:num;width:14;dec:2\" fixed=\"0\">Sum&#10;Net&#10;Sales $</th>\n" +
            "            <th name=\"catName\" type=\"a\" format=\"type:char;width:50\" fixed=\"0\">catName</th>\n" +
            "            <th name=\"catId\" type=\"i\" format=\"type:nocommas;width:10\" fixed=\"0\">catId</th>\n" +
            "        </cols>\n" +
            "        <data>\n" +
            "            <tr>\n" +
            "                <td>29.7720497414644</td>\n" +
            "                <td>10319140.4700002</td>\n" +
            "                <td>FROZEN              </td>\n" +
            "                <td>6</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>25.1500484750261</td>\n" +
            "                <td>8717131.85000016</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>7</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>13.099882331434</td>\n" +
            "                <td>4540484.35000002</td>\n" +
            "                <td>POULTRY             </td>\n" +
            "                <td>5</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>11.5210271964818</td>\n" +
            "                <td>3993245.30999995</td>\n" +
            "                <td>PAPER &amp; DISP        </td>\n" +
            "                <td>8</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>6.44327591860298</td>\n" +
            "                <td>2233271.47000001</td>\n" +
            "                <td>PRODUCE             </td>\n" +
            "                <td>11</td>\n" +
            "            </tr>\n" +
            "        </data>\n" +
            "    </table>\n" +
            "    <nrows>5</nrows>\n" +
            "</out>";

    public static String PURCHASE_DATA = "<out>\n" +
            "    <rc>0</rc>\n" +
            "    <msg>querydata successful</msg>\n" +
            "    <table>\n" +
            "        <totals>1</totals>\n" +
            "        <cols>\n" +
            "            <th name=\"oblig_dt\" type=\"i\" format=\"type:ansidate\" fixed=\"1\">Obligation&#10;Invoice&#10;Date</th>\n" +
            "            <th name=\"catgy_id\" type=\"i\" format=\"type:nocommas;width:10\" fixed=\"1\">Category ID</th>\n" +
            "            <th name=\"ec_category_description\" type=\"a\" format=\"type:char;width:50\" fixed=\"1\">Category&#10;Description</th>\n" +
            "            <th name=\"itm_nbr\" type=\"a\" format=\"type:char;width:20\" fixed=\"1\">Item&#10;Number</th>\n" +
            "            <th name=\"itm_desc_sus_itm\" type=\"a\" format=\"type:char;width:20\" fixed=\"1\">Item&#10;Description</th>\n" +
            "            <th name=\"brnd_cd_sus_itm\" type=\"a\" format=\"type:char;width:20\" fixed=\"1\">Brand&#10;Code</th>\n" +
            "            <th name=\"bic_zpaksize_sap_s_0material_attr\" type=\"a\" format=\"type:char;width:20\" fixed=\"1\">Pack/Size</th>\n" +
            "            <th name=\"itm_catch_wgt_ind\" type=\"a\" format=\"type:char;width:10\" fixed=\"1\">Item Catch&#10;Weight&#10;Indicator</th>\n" +
            "            <th name=\"case_sold_qty\" type=\"i\" format=\"type:nocommas;width:10\" fixed=\"1\">Full &#10;Cases&#10;Sold</th>\n" +
            "            <th name=\"caseequiv\" type=\"f\" format=\"type:num;width:13;dec:2\" fixed=\"0\">Sum&#10;Case&#10;Equivalents&#10;Sold</th>\n" +
            "            <th name=\"purchasedol\" type=\"f\" format=\"type:num;width:14;dec:2\" fixed=\"0\">Sum&#10;Net&#10;Sales $</th>\n" +
            "        </cols>\n" +
            "        <data>\n" +
            "            <tr>\n" +
            "                <td>20160801</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>4709913</td>\n" +
            "                <td>TOPPING SALAD WONTON STRIP</td>\n" +
            "                <td>FRSHGRM</td>\n" +
            "                <td>10/1LB</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>19</td>\n" +
            "                <td>648.77</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20160801</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>5339664</td>\n" +
            "                <td>JUICE CONC KIWI</td>\n" +
            "                <td>THREE V</td>\n" +
            "                <td>4/1GAL</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>67</td>\n" +
            "                <td>3947.49</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20160801</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>6725313</td>\n" +
            "                <td>CHIP POTATO BKD BIG GRAB</td>\n" +
            "                <td>BKDLAYS</td>\n" +
            "                <td>64/1.125Z</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>61</td>\n" +
            "                <td>1782.25</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20160801</td>\n" +
            "                <td>8</td>\n" +
            "                <td>PAPER &amp; DISP        </td>\n" +
            "                <td>7319056</td>\n" +
            "                <td>STRAW PLAS WRPD GNT 10.25</td>\n" +
            "                <td>DIXIE</td>\n" +
            "                <td>4/300CT</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>72</td>\n" +
            "                <td>1241.72</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20160801</td>\n" +
            "                <td>6</td>\n" +
            "                <td>FROZEN              </td>\n" +
            "                <td>4049195</td>\n" +
            "                <td>TORTILLA FLOUR 12IN PRSSD</td>\n" +
            "                <td>LA BAND</td>\n" +
            "                <td>8/12CT</td>\n" +
            "                <td>N</td>\n" +
            "                <td>2</td>\n" +
            "                <td>6</td>\n" +
            "                <td>103.5</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20160801</td>\n" +
            "                <td>6</td>\n" +
            "                <td>FROZEN              </td>\n" +
            "                <td>2194009</td>\n" +
            "                <td>BLUEBERRY IQF</td>\n" +
            "                <td>PACKER</td>\n" +
            "                <td>1/30LB</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>100</td>\n" +
            "                <td>4465.48</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20160801</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>8417232</td>\n" +
            "                <td>MIX COCKTAIL CRM OF COCONT CON</td>\n" +
            "                <td>THREE V</td>\n" +
            "                <td>4/1GAL</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>91</td>\n" +
            "                <td>6008.33</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20160801</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>9477498</td>\n" +
            "                <td>ALLOWANCE FOR DROP SIZE</td>\n" +
            "                <td>NONPROD</td>\n" +
            "                <td></td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>97</td>\n" +
            "                <td>-2330.82</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20160801</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>6542114</td>\n" +
            "                <td>JUICE CONC POMEGRANTE</td>\n" +
            "                <td>THREE V</td>\n" +
            "                <td>4/1GAL</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>27</td>\n" +
            "                <td>2675.88</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20160801</td>\n" +
            "                <td>3</td>\n" +
            "                <td>MEATS               </td>\n" +
            "                <td>0090811</td>\n" +
            "                <td>BACON LAYFLAT PRCK</td>\n" +
            "                <td>FARMLND</td>\n" +
            "                <td>2/150SLI</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>96</td>\n" +
            "                <td>2858.4</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20160801</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>4106498</td>\n" +
            "                <td>PINEAPPLE TIDBIT JCE FCY</td>\n" +
            "                <td>DOLE</td>\n" +
            "                <td>6/#10</td>\n" +
            "                <td>N</td>\n" +
            "                <td>4</td>\n" +
            "                <td>96</td>\n" +
            "                <td>3090.6</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20160801</td>\n" +
            "                <td>6</td>\n" +
            "                <td>FROZEN              </td>\n" +
            "                <td>3642556</td>\n" +
            "                <td>AVOCADO HALF PERU IQF</td>\n" +
            "                <td>SIMPLOT</td>\n" +
            "                <td>12/2LB</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>60</td>\n" +
            "                <td>4404.66</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20160801</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>4009189</td>\n" +
            "                <td>PEANUT BUTTER CREAMY</td>\n" +
            "                <td>SYS CLS</td>\n" +
            "                <td>6/5LB</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>42</td>\n" +
            "                <td>2299.54</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20160801</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>0593909</td>\n" +
            "                <td>JUICE LIME SELECT</td>\n" +
            "                <td>THREE V</td>\n" +
            "                <td>4/1GAL.</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>98</td>\n" +
            "                <td>6110.81</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20160801</td>\n" +
            "                <td>2</td>\n" +
            "                <td>DAIRY PRODUCTS      </td>\n" +
            "                <td>2431847</td>\n" +
            "                <td>YOGURT FROZEN ORIG</td>\n" +
            "                <td>TRPSMTH</td>\n" +
            "                <td>4/1GAL</td>\n" +
            "                <td>N</td>\n" +
            "                <td>3</td>\n" +
            "                <td>18</td>\n" +
            "                <td>727.89</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20160801</td>\n" +
            "                <td>8</td>\n" +
            "                <td>PAPER &amp; DISP        </td>\n" +
            "                <td>3086594</td>\n" +
            "                <td>LID PLAS CLR DOME LOW F/ 24 OZ</td>\n" +
            "                <td>M&amp;N</td>\n" +
            "                <td>10/100CT</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>87</td>\n" +
            "                <td>2446.98</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20160801</td>\n" +
            "                <td>8</td>\n" +
            "                <td>PAPER &amp; DISP        </td>\n" +
            "                <td>7012880</td>\n" +
            "                <td>CUP FOAM TRP SMTH FRESH 24X24</td>\n" +
            "                <td>TRPSMTH</td>\n" +
            "                <td>500/24OZ</td>\n" +
            "                <td>N</td>\n" +
            "                <td>2</td>\n" +
            "                <td>154</td>\n" +
            "                <td>6162.22</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20160801</td>\n" +
            "                <td>6</td>\n" +
            "                <td>FROZEN              </td>\n" +
            "                <td>1024439</td>\n" +
            "                <td>STRAWBERRY WHL IQF</td>\n" +
            "                <td>PACKER</td>\n" +
            "                <td>1/30LB</td>\n" +
            "                <td>N</td>\n" +
            "                <td>8</td>\n" +
            "                <td>232</td>\n" +
            "                <td>8985.04</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20160801</td>\n" +
            "                <td>6</td>\n" +
            "                <td>FROZEN              </td>\n" +
            "                <td>3333855</td>\n" +
            "                <td>TORTILLA FLOUR 12 PRSSD</td>\n" +
            "                <td>TRPSMTH</td>\n" +
            "                <td>6/12CT</td>\n" +
            "                <td>N</td>\n" +
            "                <td>0</td>\n" +
            "                <td>0</td>\n" +
            "                <td>0</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>20160801</td>\n" +
            "                <td>7</td>\n" +
            "                <td>CANNED AND DRY      </td>\n" +
            "                <td>2182388</td>\n" +
            "                <td>PEACH SLICED IRREG IN EXTRA LS</td>\n" +
            "                <td>SYS REL</td>\n" +
            "                <td>6/#10</td>\n" +
            "                <td>N</td>\n" +
            "                <td>1</td>\n" +
            "                <td>22</td>\n" +
            "                <td>860.55</td>\n" +
            "            </tr>\n" +
            "        </data>\n" +
            "    </table>\n" +
            "    <nrows>174818</nrows>\n" +
            "</out>";

    public static String TOP5_CATEGORY = "<out>\n" +
            "    <rc>0</rc>\n" +
            "    <msg>querydata successful</msg>\n" +
            "    <table>\n" +
            "        <totals>1</totals>\n" +
            "        <cols>\n" +
            "            <th name=\"date_name\" type=\"a\" fixed=\"1\">date_name</th>\n" +
            "            <th name=\"m0\" type=\"f\" format=\"type:currency\" fixed=\"0\">Category&#10;Description=PRODUCE                                           &#10;col_sort_order=1</th>\n" +
            "            <th name=\"m1\" type=\"f\" format=\"type:currency\" fixed=\"0\">Category&#10;Description=PAPER &amp; DISP                                      &#10;col_sort_order=2</th>\n" +
            "            <th name=\"m2\" type=\"f\" format=\"type:currency\" fixed=\"0\">Category&#10;Description=POULTRY                                           &#10;col_sort_order=3</th>\n" +
            "            <th name=\"m3\" type=\"f\" format=\"type:currency\" fixed=\"0\">Category&#10;Description=CANNED AND DRY                                    &#10;col_sort_order=4</th>\n" +
            "            <th name=\"m4\" type=\"f\" format=\"type:currency\" fixed=\"0\">Category&#10;Description=FROZEN                                            &#10;col_sort_order=5</th>\n" +
            "        </cols>\n" +
            "        <data>\n" +
            "            <tr>\n" +
            "                <td>2016 August-Aug &apos;16</td>\n" +
            "                <td>475614.33</td>\n" +
            "                <td>859696.8</td>\n" +
            "                <td>954003.630000001</td>\n" +
            "                <td>1864399.36999999</td>\n" +
            "                <td>2440372.95999999</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>2016 September-Sep &apos;16</td>\n" +
            "                <td>397734.01</td>\n" +
            "                <td>714356.12</td>\n" +
            "                <td>785337.42</td>\n" +
            "                <td>1551455.08999999</td>\n" +
            "                <td>1873160.71999999</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>2016 October-Oct &apos;16</td>\n" +
            "                <td>380346.34</td>\n" +
            "                <td>675050.75</td>\n" +
            "                <td>788462.19</td>\n" +
            "                <td>1487409.01999999</td>\n" +
            "                <td>1698493.80999999</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>2016 November-Nov &apos;16</td>\n" +
            "                <td>363424.27</td>\n" +
            "                <td>648075.45</td>\n" +
            "                <td>730749.34</td>\n" +
            "                <td>1394347.16999999</td>\n" +
            "                <td>1597148.33999999</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>2016 December-Dec &apos;16</td>\n" +
            "                <td>343431.37</td>\n" +
            "                <td>606758.180000001</td>\n" +
            "                <td>711513.7</td>\n" +
            "                <td>1361806.35</td>\n" +
            "                <td>1500604.08</td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>2017 January-Jan &apos;17</td>\n" +
            "                <td>272721.15</td>\n" +
            "                <td>489308.01</td>\n" +
            "                <td>570418.07</td>\n" +
            "                <td>1057714.85</td>\n" +
            "                <td>1209360.56</td>\n" +
            "            </tr>\n" +
            "        </data>\n" +
            "    </table>\n" +
            "    <nrows>6</nrows>\n" +
            "</out>";
}
