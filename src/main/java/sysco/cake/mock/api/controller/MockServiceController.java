package sysco.cake.mock.api.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import sysco.cake.mock.api.service.XmlDailyResponses;
import sysco.cake.mock.api.service.XmlMessageResponses;
import sysco.cake.mock.api.service.XmlMonthlyResponses;
import sysco.cake.mock.api.service.XmlQuarterlyResponses;
import sysco.cake.mock.api.service.XmlWeeklyResponses;
import sysco.cake.mock.api.service.XmlYearlyResponses;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This is the 1010data mock service, mainly mocks two endpoints, login and query data
 * Query data endpoint provides the data to render MSR purchase reports.
 *
 * @author Supun Muthutantrige
 * @date 1/17/17
 */
@Controller
@SpringBootApplication
public class MockServiceController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MockServiceController.class);

    String DAILY_CATEGORY = "DAILY";
    String WEEKLY_CATEGORY = "WEEKLY";
    String MONTHLY_CATEGORY = "MONTHLY";
    String QUARTERLY_CATEGORY = "QUARTERLY";
    String YEARLY_CATEGORY = "YEARLY";
    CharSequence BANNER_DATA = "label=\"Banner Data\"";
    CharSequence TOTAL_PURCHASES = "source=\"prc_ext_amt\"";
    CharSequence TOTAL_INVOICES = "source=\"oblig_nbr\"";
    CharSequence TOTAL_DELIVERIES = "source=\"rte_nbr\"";
    CharSequence TOP_CATEGORIES = "label=\"top categories\"";
    CharSequence PURCHASE_DATA = "label=\"Purchase data\"";
    CharSequence TOP5_CATEGORIES_DAY = "value=\"oblig_dt\"";
    CharSequence TOP5_CATEGORIES_WEEK = "value=\"calendar_week\"";
    CharSequence TOP5_CATEGORIES_MONTH = "value=\"calendar_month\"";
    CharSequence TOP5_CATEGORIES_QUARTER = "value=\"calendar_quarter\"";
    CharSequence TOP5_CATEGORIES_YEAR = "value=\"calendar_year\"";
    CharSequence CUS_HIE_BUSINESS_REQ = "name=\"value\" value=\"bu_nbr\"";
    CharSequence CUS_HIE_MASTER_REQ = "name=\"value\" value=\"hier_nbr\"";
    CharSequence CUS_HIE_SUB_BUSINESS_REQ = "name=\"value\" value=\"sub_bu_nbr\"";
    CharSequence DEC_ID_REQ = "name=\"value\" value=\"mua_sb_grp_id\"";
    CharSequence NAT_ID_REQ = "name=\"value\" value=\"mua_grp_id\"";
    CharSequence GRID_PUR_REPORT_REQ = "label=\"caseequiv\" breaks=\"oblig_dt,catgy_id,ec_category_description,itm_nbr,itm_desc_sus_itm,brnd_cd_sus_itm,bic_zpaksize_sap_s_0material_attr,itm_catch_wgt_ind,case_sold_qty\"";

    @RequestMapping("/")
    @ResponseBody
    String home() {
        return "Hello World!";
    }

    @RequestMapping("/ping")
    @ResponseBody
    String health() {
        return "Server is up and running";
    }

    /**
     * Returns hard coded 1010Data login credentials.
     *
     * @param apiEndpoint
     * @param apiVersion
     * @param userId
     * @param password
     * @return
     */
    @RequestMapping(value = "/prod-latest/gw.k/login",
            method = RequestMethod.GET, produces = {"application/xml"})
    @ResponseBody
    String login(@RequestParam(value = "api", required = false, defaultValue="login") String apiEndpoint,
                                @RequestParam(value = "apiversion", required = true) String apiVersion,
                                @RequestParam(value = "uid", required = true) String userId,
                                @RequestParam(value = "pswd", required = true) String password){

        LOGGER.info("Login info {}, {}, {}, {}",apiEndpoint,apiVersion,userId,password);
        return "<out><rc>0</rc><sid>1057451861</sid><pswd>_______8cb82b4f32941e1cf1b185064c6b47cf7aee961cdda6f8524309376f17e9aabaeadcc12a055aa4a379da6fea5445fa409f12975c25d31fdce0fbf1b2f3801292</pswd><msg>Last login was: 2017-01-20 08:23:32</msg><version>prod-9.64</version></out>";
    }

    /**
     * Main endpoint, which is used to render information on MSR,
     * Based on different types of requests, relevant responses are provided.
     *
     * @param apiVersion
     * @param userId
     * @param password
     * @param sessionId
     * @param xmlBody
     * @return
     * @throws InterruptedException
     * @throws ParseException
     */
    @RequestMapping(value = "/prod-latest/gw.k/querydata",
            method = RequestMethod.POST, headers="Accept=application/xml", produces = {"text/xml"})
    @ResponseBody
    public String queryTableData(@RequestParam(value = "apiversion", required = true) String apiVersion,
                                 @RequestParam(value = "uid", required = true) String userId,
                                 @RequestParam(value = "pswd", required = true) String password,
                                 @RequestParam(value = "sid", required = true) String sessionId,
                                 @RequestBody String xmlBody) throws InterruptedException, ParseException {

        LOGGER.info("Invoking QueryTableData endpoint");
        LOGGER.info("Received message ---> {}\n", xmlBody);
        Long dateRange;
        String dateRangeCategory = "NO_CATEGORY";

        dateRange = calculateDateRange(xmlBody);
        if(dateRange!=-1L){
            dateRangeCategory = assignDateRangeCategory(dateRange);
        }
        // Invoke responses based on date range
        if(DAILY_CATEGORY.equals(dateRangeCategory)){
            //Thread.sleep(1900);
            return renderDailyResponses(xmlBody);

        }else if(WEEKLY_CATEGORY.equals(dateRangeCategory)){
            //Thread.sleep(7500);
            return renderWeeklyResponses(xmlBody);

        }else if(MONTHLY_CATEGORY.equals(dateRangeCategory)){
            //Thread.sleep(7200);
            return renderMonthlyResponses(xmlBody);

        }else if (QUARTERLY_CATEGORY.equals(dateRangeCategory)){
            //Thread.sleep(7100);
            return renderQuarterlyResponses(xmlBody);

        }else if (YEARLY_CATEGORY.equals(dateRangeCategory)){
            //Thread.sleep(9500);
            return renderYearlyResponses(xmlBody);
        }

        // Invoking filter based info
        if (xmlBody.contains(CUS_HIE_BUSINESS_REQ)){
            LOGGER.info("HIERARCHEY BUSINESS RESPONSE\n");
            return XmlMessageResponses.HIERARCHEY_BUSINESS_RESPONSE;

        }else if (xmlBody.contains(CUS_HIE_MASTER_REQ)){
            LOGGER.info("HIERARCHEY MASTER RESPONSE\n");
            return XmlMessageResponses.HIERARCHEY_MASTER_RESPONSE;

        }else if (xmlBody.contains(CUS_HIE_SUB_BUSINESS_REQ)){
            LOGGER.info("HIERARCHEY SUB BUSINESS RESPONSE\n");
            return XmlMessageResponses.HIERARCHEY_SUB_BUSINESS_RESPONSE;

        }else if (xmlBody.contains(DEC_ID_REQ)){
            LOGGER.info("DECENTRALIZED ID RESPONSE\n");
            return XmlMessageResponses.DECENTRALIZED_ID_RESPONSE;

        }else if (xmlBody.contains(NAT_ID_REQ)){
            LOGGER.info("NATIONAL ID RESPONSE\n");
            return XmlMessageResponses.NATIONAL_ID_RESPONSE;

        }else if (xmlBody.contains(GRID_PUR_REPORT_REQ)){
            LOGGER.info("GRID PURCHASE RESPONSE\n");
            return XmlMessageResponses.GRID_PURCHASE_RESPONSE;

        }else {
            LOGGER.info("NO RESPONSE FOUND\n");
            return "XML request is not a defined request type";
        }
    }

    /**
     * Render responses based on daily requests for a period of 14Days
     *
     * @param xmlDailyRequest
     * @return
     * @throws InterruptedException
     */
    private String renderDailyResponses(String xmlDailyRequest) throws InterruptedException {

        LOGGER.info("Invoking queryTableData endpoint for ReportFilters Daily Category");
        LOGGER.info("Daily message request {}\n", xmlDailyRequest);

        if (xmlDailyRequest.contains(BANNER_DATA) && xmlDailyRequest.contains(TOTAL_DELIVERIES)){
            LOGGER.info("TOTAL DELIVERIES response for daily reports");
            //Thread.sleep(900);
            return XmlDailyResponses.TOTAL_DELIVERY;

        }else if (xmlDailyRequest.contains(BANNER_DATA) && xmlDailyRequest.contains(TOTAL_INVOICES)){
            LOGGER.info("TOTAL INVOICES response for daily reports");
            //Thread.sleep(600);
            return XmlDailyResponses.TOTAL_INVOICE;

        }else if (xmlDailyRequest.contains(BANNER_DATA) && xmlDailyRequest.contains(TOTAL_PURCHASES)){
            LOGGER.info("TOTAL PURCHASE response for daily reports");
            //Thread.sleep(500);
            return XmlDailyResponses.TOTAL_PURCHASE;

        }else if (xmlDailyRequest.contains(TOP_CATEGORIES)){
            LOGGER.info("TOP CATEGORIES response for daily reports");
            //Thread.sleep(1000);
            return XmlDailyResponses.TOP_CATEGORIES;

        }else if (xmlDailyRequest.contains(PURCHASE_DATA)){
            LOGGER.info("PURCHASE DATA response for daily reports");
            //Thread.sleep(1200);
            return XmlDailyResponses.PURCHASE_DATA;

        }else if (xmlDailyRequest.contains(TOP5_CATEGORIES_DAY)){
            LOGGER.info("TOP 5 CATEGORIES response for daily reports");
            //Thread.sleep(900);
            return XmlDailyResponses.TOP5_CATEGORY;

        }else{
            LOGGER.info("NO RESPONSE FOUND for DAILY reports\n");
            return "No Proper Response found";
        }
    }

    /**
     * Render responses based on weekly requests for a period of 90Days
     *
     * @param xmlWeeklyRequest
     * @return
     * @throws InterruptedException
     */
    private String renderWeeklyResponses(String xmlWeeklyRequest) throws InterruptedException {

        LOGGER.info("Invoking queryTableData endpoint for ReportFilters Weekly Category");
        LOGGER.info("Weekly message request {}\n", xmlWeeklyRequest);

        if (xmlWeeklyRequest.contains(BANNER_DATA) && xmlWeeklyRequest.contains(TOTAL_DELIVERIES)){
            LOGGER.info("TOTAL DELIVERIES response for weekly reports");
            //Thread.sleep(300);
            return XmlWeeklyResponses.TOTAL_DELIVERY;

        }else if (xmlWeeklyRequest.contains(BANNER_DATA) && xmlWeeklyRequest.contains(TOTAL_INVOICES)){
            LOGGER.info("TOTAL INVOICES response for weekly reports");
            //Thread.sleep(400);
            return XmlWeeklyResponses.TOTAL_INVOICE;

        }else if (xmlWeeklyRequest.contains(BANNER_DATA) && xmlWeeklyRequest.contains(TOTAL_PURCHASES)){
            LOGGER.info("TOTAL PURCHASE response for weekly reports");
            //Thread.sleep(500);
            return XmlWeeklyResponses.TOTAL_PURCHASE;

        }else if (xmlWeeklyRequest.contains(TOP_CATEGORIES)){
            LOGGER.info("TOP CATEGORIES response for weekly reports");
            //Thread.sleep(600);
            return XmlWeeklyResponses.TOP_CATEGORIES;

        }else if (xmlWeeklyRequest.contains(PURCHASE_DATA)){
            LOGGER.info("PURCHASE DATA response for weekly reports");
            //Thread.sleep(800);
            return XmlWeeklyResponses.PURCHASE_DATA;

        }else if (xmlWeeklyRequest.contains(TOP5_CATEGORIES_WEEK)){
            LOGGER.info("TOP 5 CATEGORIES response for weekly reports");
            //Thread.sleep(800);
            return XmlWeeklyResponses.TOP5_CATEGORY;

        }else{
            LOGGER.info("NO RESPONSE FOUND for WEEKLY reports\n");
            return "No Proper Response found";
        }
    }

    /**
     * Render responses based on monthly requests for a period of 365Days
     *
     * @param xmlMonthlyRequest
     * @return
     * @throws InterruptedException
     */
    private String renderMonthlyResponses(String xmlMonthlyRequest) throws InterruptedException {

        LOGGER.info("Invoking queryTableData endpoint for ReportFilters Monthly Category");
        LOGGER.info("Monthly message request {}\n", xmlMonthlyRequest);

        if (xmlMonthlyRequest.contains(BANNER_DATA) && xmlMonthlyRequest.contains(TOTAL_DELIVERIES)){
            LOGGER.info("TOTAL DELIVERIES response for monthly reports");
            //Thread.sleep(300);
            return XmlMonthlyResponses.TOTAL_DELIVERY;

        }else if (xmlMonthlyRequest.contains(BANNER_DATA) && xmlMonthlyRequest.contains(TOTAL_INVOICES)){
            LOGGER.info("TOTAL INVOICES response for monthly reports");
            //Thread.sleep(500);
            return XmlMonthlyResponses.TOTAL_INVOICE;

        }else if (xmlMonthlyRequest.contains(BANNER_DATA) && xmlMonthlyRequest.contains(TOTAL_PURCHASES)){
            LOGGER.info("TOTAL PURCHASE response for monthly reports");
            //Thread.sleep(300);
            return XmlMonthlyResponses.TOTAL_PURCHASE;

        }else if (xmlMonthlyRequest.contains(TOP_CATEGORIES)){
            LOGGER.info("TOP CATEGORIES response for monthly reports");
            //Thread.sleep(500);
            return XmlMonthlyResponses.TOP_CATEGORIES;

        }else if (xmlMonthlyRequest.contains(PURCHASE_DATA)){
            LOGGER.info("PURCHASE DATA response for monthly reports");
            //Thread.sleep(800);
            return XmlMonthlyResponses.PURCHASE_DATA;

        }else if (xmlMonthlyRequest.contains(TOP5_CATEGORIES_MONTH)){
            LOGGER.info("TOP 5 CATEGORIES response for monthly reports");
            //Thread.sleep(900);
            return XmlMonthlyResponses.TOP5_CATEGORY;

        }else{
            LOGGER.info("NO RESPONSE FOUND for MONTHLY reports\n");
            return "No Proper Response found";
        }
    }

    /**
     * Render responses based on quarterly requests for a period of 1095Days
     *
     * @param xmlQuarterlyRequest
     * @return
     * @throws InterruptedException
     */
    private String renderQuarterlyResponses(String xmlQuarterlyRequest) throws InterruptedException {

        LOGGER.info("Invoking queryTableData endpoint for ReportFilters Quarterly Category");
        LOGGER.info("Quarterly message request {}\n", xmlQuarterlyRequest);

        if (xmlQuarterlyRequest.contains(BANNER_DATA) && xmlQuarterlyRequest.contains(TOTAL_DELIVERIES)){
            LOGGER.info("TOTAL DELIVERIES response for quarterly reports");
            //Thread.sleep(500);
            return XmlQuarterlyResponses.TOTAL_DELIVERY;

        }else if (xmlQuarterlyRequest.contains(BANNER_DATA) && xmlQuarterlyRequest.contains(TOTAL_INVOICES)){
            LOGGER.info("TOTAL INVOICES response for quarterly reports");
            //Thread.sleep(600);
            return XmlQuarterlyResponses.TOTAL_INVOICE;

        }else if (xmlQuarterlyRequest.contains(BANNER_DATA) && xmlQuarterlyRequest.contains(TOTAL_PURCHASES)){
            LOGGER.info("TOTAL PURCHASE response for quarterly reports");
            //Thread.sleep(200);
            return XmlQuarterlyResponses.TOTAL_PURCHASE;

        }else if (xmlQuarterlyRequest.contains(TOP_CATEGORIES)){
            LOGGER.info("TOP CATEGORIES response for quarterly reports");
            //Thread.sleep(400);
            return XmlQuarterlyResponses.TOP_CATEGORIES;

        }else if (xmlQuarterlyRequest.contains(PURCHASE_DATA)){
            LOGGER.info("PURCHASE DATA response for quarterly reports");
            //Thread.sleep(800);
            return XmlQuarterlyResponses.PURCHASE_DATA;

        }else if (xmlQuarterlyRequest.contains(TOP5_CATEGORIES_QUARTER)){
            LOGGER.info("TOP 5 CATEGORIES response for quarterly reports");
            //Thread.sleep(800);
            return XmlQuarterlyResponses.TOP5_CATEGORY;

        }else{
            LOGGER.info("NO RESPONSE FOUND for QUARTERLY reports\n");
            return "No Proper Response found";
        }
    }

    /**
     * Render responses based on yearly requests for more than 1095Days
     *
     * @param xmlYearlyRequest
     * @return
     * @throws InterruptedException
     */
    private String renderYearlyResponses(String xmlYearlyRequest) throws InterruptedException {

        LOGGER.info("Invoking queryTableData endpoint for ReportFilters Yearly Category");
        LOGGER.info("Yearly message request {}\n", xmlYearlyRequest);

        if (xmlYearlyRequest.contains(BANNER_DATA) && xmlYearlyRequest.contains(TOTAL_DELIVERIES)){
            LOGGER.info("TOTAL DELIVERIES response for yearly reports");
            //Thread.sleep(400);
            return XmlYearlyResponses.TOTAL_DELIVERY;

        }else if (xmlYearlyRequest.contains(BANNER_DATA) && xmlYearlyRequest.contains(TOTAL_INVOICES)){
            LOGGER.info("TOTAL INVOICES response for yearly reports");
            //Thread.sleep(300);
            return XmlYearlyResponses.TOTAL_INVOICE;

        }else if (xmlYearlyRequest.contains(BANNER_DATA) && xmlYearlyRequest.contains(TOTAL_PURCHASES)){
            LOGGER.info("TOTAL PURCHASE response for yearly reports");
            //Thread.sleep(700);
            return XmlYearlyResponses.TOTAL_PURCHASE;

        }else if (xmlYearlyRequest.contains(TOP_CATEGORIES)){
            LOGGER.info("TOP CATEGORIES response for yearly reports");
            //Thread.sleep(700);
            return XmlYearlyResponses.TOP_CATEGORIES;

        }else if (xmlYearlyRequest.contains(PURCHASE_DATA)){
            LOGGER.info("PURCHASE DATA response for yearly reports");
            //Thread.sleep(800);
            return XmlYearlyResponses.PURCHASE_DATA;

        }else if (xmlYearlyRequest.contains(TOP5_CATEGORIES_YEAR)){
            LOGGER.info("TOP 5 CATEGORIES response for yearly reports");
            //Thread.sleep(800);
            return XmlYearlyResponses.TOP5_CATEGORY;

        }else{
            LOGGER.info("NO RESPONSE FOUND for YEARLY reports\n");
            return "No Proper Response found";
        }
    }

    /**
     * Calculates the date range of an upcoming request
     *
     * @param xmlBannerDataReq
     * @return Long dateRange
     * @throws ParseException
     */
    private Long calculateDateRange(String xmlBannerDataReq) throws ParseException {

        Pattern pattern = Pattern.compile("([0-9]{4})([0-1]{1}[1-9]{1})([0-3]{1}[0-9]{1})");
        Matcher matcher = pattern.matcher(xmlBannerDataReq);
        Set<Integer> set = new HashSet<Integer>();
        String stringDate1 = "";
        String stringDate2 = "";
        Long dateRange;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

        while (matcher.find())
        {
            set.add(Integer.parseInt(matcher.group()));
        }

        TreeSet sortedSet = new TreeSet<Integer>(set);
        if(sortedSet.size()==2){
            stringDate1 = sortedSet.first().toString();
            stringDate2 = sortedSet.last().toString();
            LOGGER.info("dates: {} {}",stringDate1,stringDate2);

        }else if(sortedSet.size()==0){
            LOGGER.info("No date range found hence returning -1");
            return -1L;
        }else{
            return 0L;
        }
        if(dateFormat.parse(stringDate1).getTime()>dateFormat.parse(stringDate2).getTime()){
            dateRange = dateFormat.parse(stringDate1).getTime() - dateFormat.parse(stringDate2).getTime();
        }else{
            dateRange = dateFormat.parse(stringDate2).getTime() - dateFormat.parse(stringDate1).getTime();
        }
        Long diffDays = dateRange / (24 * 60 * 60 * 1000);
        LOGGER.info("date range in days: {}",diffDays);
        return diffDays;
    }

    /**
     * Based on the calculated Date range, assigns a range category.
     *
     * @param dateRange
     * @return String rangeCategory
     */
    private String assignDateRangeCategory(Long dateRange){

        if(dateRange<14L){
            return DAILY_CATEGORY;
        }else if(dateRange<=90L){
            return WEEKLY_CATEGORY;
        }else if(dateRange<=365L){
            return MONTHLY_CATEGORY;
        }else if(dateRange<=1095L){
            return QUARTERLY_CATEGORY;
        }else if(dateRange>1095L){
            return YEARLY_CATEGORY;
        }else {
            return DAILY_CATEGORY;
        }
    }

    /**
     * Main methods which starts the spring boot application
     *
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(MockServiceController.class, args);
    }
}
