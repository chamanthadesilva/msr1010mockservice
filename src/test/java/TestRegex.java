import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by supun on 1/21/17.
 */
public class TestRegex {

    @Test
    public void calculateDateRange() throws ParseException {

//        String xmlMsg1 = "<in><name>sysco.views.sales</name><ops><sel value=\"mua_grp_id_customer_latest='TRSC'\"/><sel value=\"between(oblig_dt;shift(20170115;periods(20170121;20170115)-1);20170121)\"/><willbe name=\"col_name\" value=\"case(if(20170115&lt;=oblig_dt;0;1);0;'Current_range';1;'Previos_range';'Invalid_range')\"/><link col=\"customer_tenkey\" col2=\"customer_tenkey\" table2=\"sysco_customer.cust_hier\"/><link col=\"customer_tenkey\" col2=\"customer_tenkey\" table2=\"sysco_customer.cust_hier\"/><tabu cbreaks=\"col_name\" label=\"Banner Data\"><tcol format=\"type:num\" fun=\"ucnt\" name=\"value\" source=\"rte_nbr\"/></tabu></ops><format type=\"xml\"/></in>";
        String xmlMsg1 = "<in><name>sysco.views.sales</name><ops><sel value=\"mua_grp_id_customer_latest='TRSC'\"/><sel value=\"between(oblig_dt;20161223;20170121)\"/><link col=\"customer_tenkey\" col2=\"customer_tenkey\" table2=\"sysco_customer.cust_hier\"/><link col=\"customer_tenkey\" col2=\"customer_tenkey\" table2=\"sysco_customer.cust_hier\"/><tabu breaks=\"oblig_dt,catgy_id,ec_category_description,itm_nbr,itm_desc_sus_itm,brnd_cd_sus_itm,bic_zpaksize_sap_s_0material_attr,itm_catch_wgt_ind,case_sold_qty\" label=\"Purchase data\"><tcol fun=\"sum\" name=\"caseequiv\" source=\"case_eq_sold_qty\"/><tcol fun=\"sum\" name=\"purchasedol\" source=\"prc_ext_amt\"/></tabu></ops><format type=\"xml\"/><rows mode=\"2\"><from>1</from><to>20</to></rows></in>";
//        Pattern pattern = Pattern.compile("([0-9]{8})+");
        Pattern pattern = Pattern.compile("([0-9]{4})([0-1]{1}[1-9]{1})([0-3]{1}[0-9]{1})");
        Matcher matcher = pattern.matcher(xmlMsg1);
        Set<Integer> set = new HashSet<Integer>();
        String stringDate1 = "17000024";
        String stringDate2 = "20170121";
        Long dateRange;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Calendar cal1 = new GregorianCalendar();
        Calendar cal2 = new GregorianCalendar();

        while (matcher.find())
        {
            set.add(Integer.parseInt(matcher.group()));
        }

        TreeSet sortedSet = new TreeSet<Integer>(set);
        if(sortedSet.size()==2){
            stringDate1 = sortedSet.first().toString();
            stringDate2 = sortedSet.last().toString();
        }

        Date date = dateFormat.parse(stringDate1);
        System.out.println(" date:"+date);
        if(!stringDate1.equals(dateFormat.format(date))){
            System.out.println("Date is not in proper format:  "+stringDate1+" date:"+date);
        }

        System.out.println("Days= "+daysBetween(cal1.getTime(),cal2.getTime()));
    }

    public int daysBetween(Date d1, Date d2){
        return (int)( (d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
    }

    @Test
    public void assignDateRangeCategory(){
        Integer dateRange = 715;
        String dateRangeCategoryDaily = "DAILY";
        String dateRangeCategoryWeekly = "WEEKLY";
        String dateRangeCategoryMonthly = "MONTHLY";
        String dateRangeCategoryQuarterly = "QUARTERLY";
        String dateRangeCategoryYearly = "YEARLY";
        String assignRange = "DAILY";

        //Should Return
        if(dateRange<=30){
            // < 1 month ~ 31 days
            assignRange = dateRangeCategoryDaily;

        }else if(dateRange<=90){
            // < 3 months ~ 90 days
            assignRange = dateRangeCategoryWeekly;

        }else if(dateRange<=180){
            // < 6 months ~ 180 days
            assignRange = dateRangeCategoryMonthly;

        }else if(dateRange<=720){
            // < 24 months ~ 720 days
            assignRange = dateRangeCategoryQuarterly;

        }else if(dateRange>720){
            // > 24 months ~ 720 days
            assignRange = dateRangeCategoryYearly;
        }

        System.out.println(assignRange);
//        Assert.assertEquals("Range check",DAILY_CATEGORY,assignRange);
        Assert.assertEquals("Range check",dateRangeCategoryQuarterly,assignRange);
    }
}
